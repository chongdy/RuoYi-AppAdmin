package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 举报对象 app_report
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class Report extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long reportId;

    /** 举报者 */
    @Excel(name = "举报者")
    private String memberId;

    /** 举报对象 */
    @Excel(name = "举报对象")
    private String memberIdTo;

    /** 举报备注 */
    @Excel(name = "举报备注")
    private String report;

    /** 举报类型0-虚假会员，1-推销广告，2-是个托，3-其他 */
    @Excel(name = "举报类型0-虚假会员，1-推销广告，2-是个托，3-其他")
    private Integer reportType;

    public void setReportId(Long reportId) 
    {
        this.reportId = reportId;
    }

    public Long getReportId() 
    {
        return reportId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdTo(String memberIdTo) 
    {
        this.memberIdTo = memberIdTo;
    }

    public String getMemberIdTo() 
    {
        return memberIdTo;
    }
    public void setReport(String report) 
    {
        this.report = report;
    }

    public String getReport() 
    {
        return report;
    }
    public void setReportType(Integer reportType) 
    {
        this.reportType = reportType;
    }

    public Integer getReportType() 
    {
        return reportType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("reportId", getReportId())
            .append("memberId", getMemberId())
            .append("memberIdTo", getMemberIdTo())
            .append("report", getReport())
            .append("createTime", getCreateTime())
            .append("reportType", getReportType())
            .toString();
    }
}
