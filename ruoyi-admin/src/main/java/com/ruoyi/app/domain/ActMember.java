package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报名记录对象 app_act_member
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class ActMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String acmId;

    /** 活动ID */
    @Excel(name = "活动ID")
    private String actId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 状态0-待审核，1-审核通过，2-未通过 */
    @Excel(name = "状态0-待审核，1-审核通过，2-未通过")
    private Integer state;

    /** 姓名 */
    @Excel(name = "姓名")
    private String username;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String cardNo;

    /** 电话号码 */
    @Excel(name = "电话号码")
    private String phone;

    /** 未通过原因 */
    @Excel(name = "未通过原因")
    private String notPassMsg;

    /** 联系微信 */
    @Excel(name = "联系微信")
    private String linkmanWx;

    /** 是否领队 */
    @Excel(name = "是否领队")
    private Integer areLeader;

    /** 是否申请领队 */
    @Excel(name = "是否申请领队")
    private Integer applyLeader;

    public void setAcmId(String acmId) 
    {
        this.acmId = acmId;
    }

    public String getAcmId() 
    {
        return acmId;
    }
    public void setActId(String actId) 
    {
        this.actId = actId;
    }

    public String getActId() 
    {
        return actId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setCardNo(String cardNo) 
    {
        this.cardNo = cardNo;
    }

    public String getCardNo() 
    {
        return cardNo;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setNotPassMsg(String notPassMsg) 
    {
        this.notPassMsg = notPassMsg;
    }

    public String getNotPassMsg() 
    {
        return notPassMsg;
    }
    public void setLinkmanWx(String linkmanWx) 
    {
        this.linkmanWx = linkmanWx;
    }

    public String getLinkmanWx() 
    {
        return linkmanWx;
    }
    public void setAreLeader(Integer areLeader) 
    {
        this.areLeader = areLeader;
    }

    public Integer getAreLeader() 
    {
        return areLeader;
    }
    public void setApplyLeader(Integer applyLeader) 
    {
        this.applyLeader = applyLeader;
    }

    public Integer getApplyLeader() 
    {
        return applyLeader;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("acmId", getAcmId())
            .append("actId", getActId())
            .append("memberId", getMemberId())
            .append("state", getState())
            .append("username", getUsername())
            .append("cardNo", getCardNo())
            .append("phone", getPhone())
            .append("createTime", getCreateTime())
            .append("notPassMsg", getNotPassMsg())
            .append("linkmanWx", getLinkmanWx())
            .append("areLeader", getAreLeader())
            .append("applyLeader", getApplyLeader())
            .toString();
    }
}
