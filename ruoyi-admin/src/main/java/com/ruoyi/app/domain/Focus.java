package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 关注对象 app_focus
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class Focus extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long focusId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 关注ID */
    @Excel(name = "关注ID")
    private String memberIdFocus;

    public void setFocusId(Long focusId) 
    {
        this.focusId = focusId;
    }

    public Long getFocusId() 
    {
        return focusId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdFocus(String memberIdFocus) 
    {
        this.memberIdFocus = memberIdFocus;
    }

    public String getMemberIdFocus() 
    {
        return memberIdFocus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("focusId", getFocusId())
            .append("memberId", getMemberId())
            .append("memberIdFocus", getMemberIdFocus())
            .append("createTime", getCreateTime())
            .toString();
    }
}
