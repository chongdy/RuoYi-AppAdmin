package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 加群记录对象 app_group_member
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class GroupMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String agmId;

    /** 群ID */
    @Excel(name = "群ID")
    private String groupId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 状态0-待审核，1-审核通过，2-未通过 */
    @Excel(name = "状态0-待审核，1-审核通过，2-未通过")
    private Integer state;

    /** 未通过原因 */
    @Excel(name = "未通过原因")
    private String notPassMsg;

    public void setAgmId(String agmId) 
    {
        this.agmId = agmId;
    }

    public String getAgmId() 
    {
        return agmId;
    }
    public void setGroupId(String groupId) 
    {
        this.groupId = groupId;
    }

    public String getGroupId() 
    {
        return groupId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setNotPassMsg(String notPassMsg) 
    {
        this.notPassMsg = notPassMsg;
    }

    public String getNotPassMsg() 
    {
        return notPassMsg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("agmId", getAgmId())
            .append("groupId", getGroupId())
            .append("memberId", getMemberId())
            .append("state", getState())
            .append("createTime", getCreateTime())
            .append("notPassMsg", getNotPassMsg())
            .toString();
    }
}
