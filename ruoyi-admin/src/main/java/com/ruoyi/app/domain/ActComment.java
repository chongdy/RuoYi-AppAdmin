package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 活动评价对象 app_act_comment
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class ActComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long actCommId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 价值分 */
    @Excel(name = "价值分")
    private Integer valueScore;

    /** 好玩分 */
    @Excel(name = "好玩分")
    private Integer goodPlayScore;

    /** 活动ID */
    @Excel(name = "活动ID")
    private String actId;

    /** 评论建议 */
    @Excel(name = "评论建议")
    private String commSuggest;

    /** 评论标签集 */
    @Excel(name = "评论标签集")
    private String actCommTag;

    public void setActCommId(Long actCommId) 
    {
        this.actCommId = actCommId;
    }

    public Long getActCommId() 
    {
        return actCommId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setValueScore(Integer valueScore) 
    {
        this.valueScore = valueScore;
    }

    public Integer getValueScore() 
    {
        return valueScore;
    }
    public void setGoodPlayScore(Integer goodPlayScore) 
    {
        this.goodPlayScore = goodPlayScore;
    }

    public Integer getGoodPlayScore() 
    {
        return goodPlayScore;
    }
    public void setActId(String actId) 
    {
        this.actId = actId;
    }

    public String getActId() 
    {
        return actId;
    }
    public void setCommSuggest(String commSuggest) 
    {
        this.commSuggest = commSuggest;
    }

    public String getCommSuggest() 
    {
        return commSuggest;
    }
    public void setActCommTag(String actCommTag) 
    {
        this.actCommTag = actCommTag;
    }

    public String getActCommTag() 
    {
        return actCommTag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("actCommId", getActCommId())
            .append("memberId", getMemberId())
            .append("valueScore", getValueScore())
            .append("goodPlayScore", getGoodPlayScore())
            .append("createTime", getCreateTime())
            .append("actId", getActId())
            .append("commSuggest", getCommSuggest())
            .append("actCommTag", getActCommTag())
            .toString();
    }
}
