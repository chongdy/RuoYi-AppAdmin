package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 游记类型对象 app_note_type
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class NoteType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long noteTypeId;

    /** 父ID */
    @Excel(name = "父ID")
    private Long noteTypePid;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 子标题 */
    @Excel(name = "子标题")
    private String subTitle;

    /** 描述 */
    @Excel(name = "描述")
    private String typeDesc;

    /** 图标 */
    @Excel(name = "图标")
    private String pic;

    /** 分享标题 */
    @Excel(name = "分享标题")
    private String shareTitle;

    /** 分享内容 */
    @Excel(name = "分享内容")
    private String shareDescription;

    /** 分享图片 */
    @Excel(name = "分享图片")
    private String shareImg;

    /** 分享链接 */
    @Excel(name = "分享链接")
    private String shareUrl;

    /** 展示顺序 */
    @Excel(name = "展示顺序")
    private Long sortNum;

    /** 是否首页展示0-否;1-是 */
    @Excel(name = "是否首页展示0-否;1-是")
    private Integer showHome;

    public void setNoteTypeId(Long noteTypeId) 
    {
        this.noteTypeId = noteTypeId;
    }

    public Long getNoteTypeId() 
    {
        return noteTypeId;
    }
    public void setNoteTypePid(Long noteTypePid) 
    {
        this.noteTypePid = noteTypePid;
    }

    public Long getNoteTypePid() 
    {
        return noteTypePid;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setTypeDesc(String typeDesc) 
    {
        this.typeDesc = typeDesc;
    }

    public String getTypeDesc() 
    {
        return typeDesc;
    }
    public void setPic(String pic) 
    {
        this.pic = pic;
    }

    public String getPic() 
    {
        return pic;
    }
    public void setShareTitle(String shareTitle) 
    {
        this.shareTitle = shareTitle;
    }

    public String getShareTitle() 
    {
        return shareTitle;
    }
    public void setShareDescription(String shareDescription) 
    {
        this.shareDescription = shareDescription;
    }

    public String getShareDescription() 
    {
        return shareDescription;
    }
    public void setShareImg(String shareImg) 
    {
        this.shareImg = shareImg;
    }

    public String getShareImg() 
    {
        return shareImg;
    }
    public void setShareUrl(String shareUrl) 
    {
        this.shareUrl = shareUrl;
    }

    public String getShareUrl() 
    {
        return shareUrl;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }
    public void setShowHome(Integer showHome) 
    {
        this.showHome = showHome;
    }

    public Integer getShowHome() 
    {
        return showHome;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("noteTypeId", getNoteTypeId())
            .append("noteTypePid", getNoteTypePid())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("typeDesc", getTypeDesc())
            .append("pic", getPic())
            .append("createTime", getCreateTime())
            .append("shareTitle", getShareTitle())
            .append("shareDescription", getShareDescription())
            .append("shareImg", getShareImg())
            .append("shareUrl", getShareUrl())
            .append("sortNum", getSortNum())
            .append("showHome", getShowHome())
            .toString();
    }
}
