package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 广场不感兴趣对象 app_plaza_nofeel_tag
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class PlazaNofeelTag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String nofeelId;

    /** 无兴趣tag */
    @Excel(name = "无兴趣tag")
    private String nofeelTag;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    public void setNofeelId(String nofeelId) 
    {
        this.nofeelId = nofeelId;
    }

    public String getNofeelId() 
    {
        return nofeelId;
    }
    public void setNofeelTag(String nofeelTag) 
    {
        this.nofeelTag = nofeelTag;
    }

    public String getNofeelTag() 
    {
        return nofeelTag;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("nofeelId", getNofeelId())
            .append("nofeelTag", getNofeelTag())
            .append("memberId", getMemberId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
