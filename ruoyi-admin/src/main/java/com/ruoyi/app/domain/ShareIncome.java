package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分享新用户收入对象 app_share_income
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class ShareIncome extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** note_income_id */
    private String shareIncomeId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 新用户ID */
    @Excel(name = "新用户ID")
    private String newMemberId;

    /** 状态0-审核不通过，1-审核通过，2-待审核 */
    @Excel(name = "状态0-审核不通过，1-审核通过，2-待审核")
    private Integer state;

    /** 收入趣币 */
    @Excel(name = "收入趣币")
    private Long incomeCoin;

    public void setShareIncomeId(String shareIncomeId) 
    {
        this.shareIncomeId = shareIncomeId;
    }

    public String getShareIncomeId() 
    {
        return shareIncomeId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setNewMemberId(String newMemberId) 
    {
        this.newMemberId = newMemberId;
    }

    public String getNewMemberId() 
    {
        return newMemberId;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setIncomeCoin(Long incomeCoin) 
    {
        this.incomeCoin = incomeCoin;
    }

    public Long getIncomeCoin() 
    {
        return incomeCoin;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("shareIncomeId", getShareIncomeId())
            .append("memberId", getMemberId())
            .append("newMemberId", getNewMemberId())
            .append("state", getState())
            .append("createTime", getCreateTime())
            .append("incomeCoin", getIncomeCoin())
            .toString();
    }
}
