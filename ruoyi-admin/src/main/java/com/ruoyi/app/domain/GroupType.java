package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 群类型对象 app_group_type
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class GroupType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long groupTypeId;

    /** 父ID */
    @Excel(name = "父ID")
    private Long groupTypePid;

    /** 展示顺序 */
    @Excel(name = "展示顺序")
    private Long sortNum;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 子标题 */
    @Excel(name = "子标题")
    private String subTitle;

    /** 描述 */
    @Excel(name = "描述")
    private String typeDesc;

    /** 图标 */
    @Excel(name = "图标")
    private String pic;

    /** 分享标题 */
    @Excel(name = "分享标题")
    private String shareTitle;

    /** 分享内容 */
    @Excel(name = "分享内容")
    private String shareDescription;

    /** 分享图片 */
    @Excel(name = "分享图片")
    private String shareImg;

    /** 分享链接 */
    @Excel(name = "分享链接")
    private String shareUrl;

    /** 海报模板 */
    @Excel(name = "海报模板")
    private String posterPsd;

    /** 封面参考，逗号隔开 */
    @Excel(name = "封面参考，逗号隔开")
    private String coverPsd;

    public void setGroupTypeId(Long groupTypeId) 
    {
        this.groupTypeId = groupTypeId;
    }

    public Long getGroupTypeId() 
    {
        return groupTypeId;
    }
    public void setGroupTypePid(Long groupTypePid) 
    {
        this.groupTypePid = groupTypePid;
    }

    public Long getGroupTypePid() 
    {
        return groupTypePid;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setTypeDesc(String typeDesc) 
    {
        this.typeDesc = typeDesc;
    }

    public String getTypeDesc() 
    {
        return typeDesc;
    }
    public void setPic(String pic) 
    {
        this.pic = pic;
    }

    public String getPic() 
    {
        return pic;
    }
    public void setShareTitle(String shareTitle) 
    {
        this.shareTitle = shareTitle;
    }

    public String getShareTitle() 
    {
        return shareTitle;
    }
    public void setShareDescription(String shareDescription) 
    {
        this.shareDescription = shareDescription;
    }

    public String getShareDescription() 
    {
        return shareDescription;
    }
    public void setShareImg(String shareImg) 
    {
        this.shareImg = shareImg;
    }

    public String getShareImg() 
    {
        return shareImg;
    }
    public void setShareUrl(String shareUrl) 
    {
        this.shareUrl = shareUrl;
    }

    public String getShareUrl() 
    {
        return shareUrl;
    }
    public void setPosterPsd(String posterPsd) 
    {
        this.posterPsd = posterPsd;
    }

    public String getPosterPsd() 
    {
        return posterPsd;
    }
    public void setCoverPsd(String coverPsd) 
    {
        this.coverPsd = coverPsd;
    }

    public String getCoverPsd() 
    {
        return coverPsd;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("groupTypeId", getGroupTypeId())
            .append("groupTypePid", getGroupTypePid())
            .append("sortNum", getSortNum())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("typeDesc", getTypeDesc())
            .append("pic", getPic())
            .append("createTime", getCreateTime())
            .append("shareTitle", getShareTitle())
            .append("shareDescription", getShareDescription())
            .append("shareImg", getShareImg())
            .append("shareUrl", getShareUrl())
            .append("posterPsd", getPosterPsd())
            .append("coverPsd", getCoverPsd())
            .toString();
    }
}
