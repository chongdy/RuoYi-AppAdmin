package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员黑名单对象 app_member_blacklist
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class MemberBlacklist extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long blackId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 拉黑会员ID */
    @Excel(name = "拉黑会员ID")
    private String memberIdBlack;

    public void setBlackId(Long blackId) 
    {
        this.blackId = blackId;
    }

    public Long getBlackId() 
    {
        return blackId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdBlack(String memberIdBlack) 
    {
        this.memberIdBlack = memberIdBlack;
    }

    public String getMemberIdBlack() 
    {
        return memberIdBlack;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("blackId", getBlackId())
            .append("memberId", getMemberId())
            .append("memberIdBlack", getMemberIdBlack())
            .append("createTime", getCreateTime())
            .toString();
    }
}
