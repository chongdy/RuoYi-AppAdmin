package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员评价对象 app_member_comment
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class MemberComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long memCommId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 颜值分 */
    @Excel(name = "颜值分")
    private Integer faceScore;

    /** 人品分 */
    @Excel(name = "人品分")
    private Integer qualityScore;

    /** 绅士分 */
    @Excel(name = "绅士分")
    private Integer gentryScore;

    /** 评论会员 */
    @Excel(name = "评论会员")
    private String memberIdTo;

    /** 评论建议 */
    @Excel(name = "评论建议")
    private String commSuggest;

    /** 评论标签集 */
    @Excel(name = "评论标签集")
    private String memCommTag;

    /** 是否展示 */
    @Excel(name = "是否展示")
    private Integer canShow;

    /** 赞同数 */
    @Excel(name = "赞同数")
    private Long supportNum;

    /** 反对数 */
    @Excel(name = "反对数")
    private Long notSupportNum;

    /** 展示顺序 */
    @Excel(name = "展示顺序")
    private Long sortNum;

    public void setMemCommId(Long memCommId) 
    {
        this.memCommId = memCommId;
    }

    public Long getMemCommId() 
    {
        return memCommId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setFaceScore(Integer faceScore) 
    {
        this.faceScore = faceScore;
    }

    public Integer getFaceScore() 
    {
        return faceScore;
    }
    public void setQualityScore(Integer qualityScore) 
    {
        this.qualityScore = qualityScore;
    }

    public Integer getQualityScore() 
    {
        return qualityScore;
    }
    public void setGentryScore(Integer gentryScore) 
    {
        this.gentryScore = gentryScore;
    }

    public Integer getGentryScore() 
    {
        return gentryScore;
    }
    public void setMemberIdTo(String memberIdTo) 
    {
        this.memberIdTo = memberIdTo;
    }

    public String getMemberIdTo() 
    {
        return memberIdTo;
    }
    public void setCommSuggest(String commSuggest) 
    {
        this.commSuggest = commSuggest;
    }

    public String getCommSuggest() 
    {
        return commSuggest;
    }
    public void setMemCommTag(String memCommTag) 
    {
        this.memCommTag = memCommTag;
    }

    public String getMemCommTag() 
    {
        return memCommTag;
    }
    public void setCanShow(Integer canShow) 
    {
        this.canShow = canShow;
    }

    public Integer getCanShow() 
    {
        return canShow;
    }
    public void setSupportNum(Long supportNum) 
    {
        this.supportNum = supportNum;
    }

    public Long getSupportNum() 
    {
        return supportNum;
    }
    public void setNotSupportNum(Long notSupportNum) 
    {
        this.notSupportNum = notSupportNum;
    }

    public Long getNotSupportNum() 
    {
        return notSupportNum;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("memCommId", getMemCommId())
            .append("memberId", getMemberId())
            .append("faceScore", getFaceScore())
            .append("qualityScore", getQualityScore())
            .append("gentryScore", getGentryScore())
            .append("createTime", getCreateTime())
            .append("memberIdTo", getMemberIdTo())
            .append("commSuggest", getCommSuggest())
            .append("memCommTag", getMemCommTag())
            .append("canShow", getCanShow())
            .append("supportNum", getSupportNum())
            .append("notSupportNum", getNotSupportNum())
            .append("sortNum", getSortNum())
            .toString();
    }
}
