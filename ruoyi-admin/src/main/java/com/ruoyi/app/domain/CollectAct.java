package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收藏活动对象 app_collect_act
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class CollectAct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 收藏ID */
    private Long collectId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 活动ID */
    @Excel(name = "活动ID")
    private String actIdCollect;

    public void setCollectId(Long collectId) 
    {
        this.collectId = collectId;
    }

    public Long getCollectId() 
    {
        return collectId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setActIdCollect(String actIdCollect) 
    {
        this.actIdCollect = actIdCollect;
    }

    public String getActIdCollect() 
    {
        return actIdCollect;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("collectId", getCollectId())
            .append("memberId", getMemberId())
            .append("createTime", getCreateTime())
            .append("actIdCollect", getActIdCollect())
            .toString();
    }
}
