package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 游记收入对象 app_note_income
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class NoteIncome extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** note_income_id */
    private String noteIncomeId;

    /** 游记ID */
    @Excel(name = "游记ID")
    private String noteId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 聚会ID */
    @Excel(name = "聚会ID")
    private String actId;

    /** 状态0-审核不通过，1-审核通过 */
    @Excel(name = "状态0-审核不通过，1-审核通过")
    private Integer state;

    /** 总费用 */
    @Excel(name = "总费用")
    private Long totalPrice;

    /** 收入趣币 */
    @Excel(name = "收入趣币")
    private Long incomeCoin;

    public void setNoteIncomeId(String noteIncomeId) 
    {
        this.noteIncomeId = noteIncomeId;
    }

    public String getNoteIncomeId() 
    {
        return noteIncomeId;
    }
    public void setNoteId(String noteId) 
    {
        this.noteId = noteId;
    }

    public String getNoteId() 
    {
        return noteId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setActId(String actId) 
    {
        this.actId = actId;
    }

    public String getActId() 
    {
        return actId;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setTotalPrice(Long totalPrice) 
    {
        this.totalPrice = totalPrice;
    }

    public Long getTotalPrice() 
    {
        return totalPrice;
    }
    public void setIncomeCoin(Long incomeCoin) 
    {
        this.incomeCoin = incomeCoin;
    }

    public Long getIncomeCoin() 
    {
        return incomeCoin;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("noteIncomeId", getNoteIncomeId())
            .append("noteId", getNoteId())
            .append("memberId", getMemberId())
            .append("actId", getActId())
            .append("state", getState())
            .append("createTime", getCreateTime())
            .append("totalPrice", getTotalPrice())
            .append("incomeCoin", getIncomeCoin())
            .toString();
    }
}
