package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 通知对象 app_notify_msg
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class NotifyMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 通知ID */
    private String notifyId;

    /** 通知类型0-所有类，1特定会员集，2-系统通知 */
    @Excel(name = "通知类型0-所有类，1特定会员集，2-系统通知")
    private String notifyType;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 会员ID集 */
    @Excel(name = "会员ID集")
    private String memberIds;

    /** 状态，0-未查看，1-已查看，【2-启用，3-未启用针对notify_type=2】 */
    @Excel(name = "状态，0-未查看，1-已查看，【2-启用，3-未启用针对notify_type=2】")
    private Integer state;

    public void setNotifyId(String notifyId) 
    {
        this.notifyId = notifyId;
    }

    public String getNotifyId() 
    {
        return notifyId;
    }
    public void setNotifyType(String notifyType) 
    {
        this.notifyType = notifyType;
    }

    public String getNotifyType() 
    {
        return notifyType;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setMemberIds(String memberIds) 
    {
        this.memberIds = memberIds;
    }

    public String getMemberIds() 
    {
        return memberIds;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("notifyId", getNotifyId())
            .append("notifyType", getNotifyType())
            .append("content", getContent())
            .append("memberIds", getMemberIds())
            .append("createTime", getCreateTime())
            .append("state", getState())
            .toString();
    }
}
