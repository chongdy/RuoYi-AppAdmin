package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.ActType;
import com.ruoyi.app.service.IActTypeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 活动类型Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/actType")
public class ActTypeController extends BaseController
{
    private String prefix = "app/actType";

    @Autowired
    private IActTypeService actTypeService;

    @RequiresPermissions("app:actType:view")
    @GetMapping()
    public String actType()
    {
        return prefix + "/actType";
    }

    /**
     * 查询活动类型列表
     */
    @RequiresPermissions("app:actType:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ActType actType)
    {
        startPage();
        List<ActType> list = actTypeService.selectActTypeList(actType);
        return getDataTable(list);
    }

    /**
     * 导出活动类型列表
     */
    @RequiresPermissions("app:actType:export")
    @Log(title = "活动类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ActType actType)
    {
        List<ActType> list = actTypeService.selectActTypeList(actType);
        ExcelUtil<ActType> util = new ExcelUtil<ActType>(ActType.class);
        return util.exportExcel(list, "actType");
    }

    /**
     * 新增活动类型
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存活动类型
     */
    @RequiresPermissions("app:actType:add")
    @Log(title = "活动类型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ActType actType)
    {
        return toAjax(actTypeService.insertActType(actType));
    }

    /**
     * 修改活动类型
     */
    @GetMapping("/edit/{actTypeId}")
    public String edit(@PathVariable("actTypeId") Long actTypeId, ModelMap mmap)
    {
        ActType actType = actTypeService.selectActTypeById(actTypeId);
        mmap.put("actType", actType);
        return prefix + "/edit";
    }

    /**
     * 修改保存活动类型
     */
    @RequiresPermissions("app:actType:edit")
    @Log(title = "活动类型", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ActType actType)
    {
        return toAjax(actTypeService.updateActType(actType));
    }

    /**
     * 删除活动类型
     */
    @RequiresPermissions("app:actType:remove")
    @Log(title = "活动类型", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(actTypeService.deleteActTypeByIds(ids));
    }
}
