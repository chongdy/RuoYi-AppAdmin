package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.VersionRec;
import com.ruoyi.app.service.IVersionRecService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * app版本Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/versionRec")
public class VersionRecController extends BaseController
{
    private String prefix = "app/versionRec";

    @Autowired
    private IVersionRecService versionRecService;

    @RequiresPermissions("app:versionRec:view")
    @GetMapping()
    public String versionRec()
    {
        return prefix + "/versionRec";
    }

    /**
     * 查询app版本列表
     */
    @RequiresPermissions("app:versionRec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(VersionRec versionRec)
    {
        startPage();
        List<VersionRec> list = versionRecService.selectVersionRecList(versionRec);
        return getDataTable(list);
    }

    /**
     * 导出app版本列表
     */
    @RequiresPermissions("app:versionRec:export")
    @Log(title = "app版本", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(VersionRec versionRec)
    {
        List<VersionRec> list = versionRecService.selectVersionRecList(versionRec);
        ExcelUtil<VersionRec> util = new ExcelUtil<VersionRec>(VersionRec.class);
        return util.exportExcel(list, "versionRec");
    }

    /**
     * 新增app版本
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存app版本
     */
    @RequiresPermissions("app:versionRec:add")
    @Log(title = "app版本", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(VersionRec versionRec)
    {
        return toAjax(versionRecService.insertVersionRec(versionRec));
    }

    /**
     * 修改app版本
     */
    @GetMapping("/edit/{verId}")
    public String edit(@PathVariable("verId") String verId, ModelMap mmap)
    {
        VersionRec versionRec = versionRecService.selectVersionRecById(verId);
        mmap.put("versionRec", versionRec);
        return prefix + "/edit";
    }

    /**
     * 修改保存app版本
     */
    @RequiresPermissions("app:versionRec:edit")
    @Log(title = "app版本", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(VersionRec versionRec)
    {
        return toAjax(versionRecService.updateVersionRec(versionRec));
    }

    /**
     * 删除app版本
     */
    @RequiresPermissions("app:versionRec:remove")
    @Log(title = "app版本", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(versionRecService.deleteVersionRecByIds(ids));
    }
}
