package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.LevelInfo;
import com.ruoyi.app.service.ILevelInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 等级Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/levelInfo")
public class LevelInfoController extends BaseController
{
    private String prefix = "app/levelInfo";

    @Autowired
    private ILevelInfoService levelInfoService;

    @RequiresPermissions("app:levelInfo:view")
    @GetMapping()
    public String levelInfo()
    {
        return prefix + "/levelInfo";
    }

    /**
     * 查询等级列表
     */
    @RequiresPermissions("app:levelInfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LevelInfo levelInfo)
    {
        startPage();
        List<LevelInfo> list = levelInfoService.selectLevelInfoList(levelInfo);
        return getDataTable(list);
    }

    /**
     * 导出等级列表
     */
    @RequiresPermissions("app:levelInfo:export")
    @Log(title = "等级", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LevelInfo levelInfo)
    {
        List<LevelInfo> list = levelInfoService.selectLevelInfoList(levelInfo);
        ExcelUtil<LevelInfo> util = new ExcelUtil<LevelInfo>(LevelInfo.class);
        return util.exportExcel(list, "levelInfo");
    }

    /**
     * 新增等级
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存等级
     */
    @RequiresPermissions("app:levelInfo:add")
    @Log(title = "等级", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(LevelInfo levelInfo)
    {
        return toAjax(levelInfoService.insertLevelInfo(levelInfo));
    }

    /**
     * 修改等级
     */
    @GetMapping("/edit/{levelId}")
    public String edit(@PathVariable("levelId") String levelId, ModelMap mmap)
    {
        LevelInfo levelInfo = levelInfoService.selectLevelInfoById(levelId);
        mmap.put("levelInfo", levelInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存等级
     */
    @RequiresPermissions("app:levelInfo:edit")
    @Log(title = "等级", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LevelInfo levelInfo)
    {
        return toAjax(levelInfoService.updateLevelInfo(levelInfo));
    }

    /**
     * 删除等级
     */
    @RequiresPermissions("app:levelInfo:remove")
    @Log(title = "等级", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(levelInfoService.deleteLevelInfoByIds(ids));
    }
}
