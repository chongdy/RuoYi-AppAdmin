package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MemberCommentTag;
import com.ruoyi.app.service.IMemberCommentTagService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员评价标签Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/memberCommentTag")
public class MemberCommentTagController extends BaseController
{
    private String prefix = "app/memberCommentTag";

    @Autowired
    private IMemberCommentTagService memberCommentTagService;

    @RequiresPermissions("app:memberCommentTag:view")
    @GetMapping()
    public String memberCommentTag()
    {
        return prefix + "/memberCommentTag";
    }

    /**
     * 查询会员评价标签列表
     */
    @RequiresPermissions("app:memberCommentTag:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberCommentTag memberCommentTag)
    {
        startPage();
        List<MemberCommentTag> list = memberCommentTagService.selectMemberCommentTagList(memberCommentTag);
        return getDataTable(list);
    }

    /**
     * 导出会员评价标签列表
     */
    @RequiresPermissions("app:memberCommentTag:export")
    @Log(title = "会员评价标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberCommentTag memberCommentTag)
    {
        List<MemberCommentTag> list = memberCommentTagService.selectMemberCommentTagList(memberCommentTag);
        ExcelUtil<MemberCommentTag> util = new ExcelUtil<MemberCommentTag>(MemberCommentTag.class);
        return util.exportExcel(list, "memberCommentTag");
    }

    /**
     * 新增会员评价标签
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存会员评价标签
     */
    @RequiresPermissions("app:memberCommentTag:add")
    @Log(title = "会员评价标签", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberCommentTag memberCommentTag)
    {
        return toAjax(memberCommentTagService.insertMemberCommentTag(memberCommentTag));
    }

    /**
     * 修改会员评价标签
     */
    @GetMapping("/edit/{tagId}")
    public String edit(@PathVariable("tagId") Long tagId, ModelMap mmap)
    {
        MemberCommentTag memberCommentTag = memberCommentTagService.selectMemberCommentTagById(tagId);
        mmap.put("memberCommentTag", memberCommentTag);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员评价标签
     */
    @RequiresPermissions("app:memberCommentTag:edit")
    @Log(title = "会员评价标签", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberCommentTag memberCommentTag)
    {
        return toAjax(memberCommentTagService.updateMemberCommentTag(memberCommentTag));
    }

    /**
     * 删除会员评价标签
     */
    @RequiresPermissions("app:memberCommentTag:remove")
    @Log(title = "会员评价标签", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberCommentTagService.deleteMemberCommentTagByIds(ids));
    }
}
