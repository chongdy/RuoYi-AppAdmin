package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MerchantInfo;
import com.ruoyi.app.service.IMerchantInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商家Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/merchantInfo")
public class MerchantInfoController extends BaseController
{
    private String prefix = "app/merchantInfo";

    @Autowired
    private IMerchantInfoService merchantInfoService;

    @RequiresPermissions("app:merchantInfo:view")
    @GetMapping()
    public String merchantInfo()
    {
        return prefix + "/merchantInfo";
    }

    /**
     * 查询商家列表
     */
    @RequiresPermissions("app:merchantInfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MerchantInfo merchantInfo)
    {
        startPage();
        List<MerchantInfo> list = merchantInfoService.selectMerchantInfoList(merchantInfo);
        return getDataTable(list);
    }

    /**
     * 导出商家列表
     */
    @RequiresPermissions("app:merchantInfo:export")
    @Log(title = "商家", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MerchantInfo merchantInfo)
    {
        List<MerchantInfo> list = merchantInfoService.selectMerchantInfoList(merchantInfo);
        ExcelUtil<MerchantInfo> util = new ExcelUtil<MerchantInfo>(MerchantInfo.class);
        return util.exportExcel(list, "merchantInfo");
    }

    /**
     * 新增商家
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商家
     */
    @RequiresPermissions("app:merchantInfo:add")
    @Log(title = "商家", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MerchantInfo merchantInfo)
    {
        return toAjax(merchantInfoService.insertMerchantInfo(merchantInfo));
    }

    /**
     * 修改商家
     */
    @GetMapping("/edit/{merId}")
    public String edit(@PathVariable("merId") String merId, ModelMap mmap)
    {
        MerchantInfo merchantInfo = merchantInfoService.selectMerchantInfoById(merId);
        mmap.put("merchantInfo", merchantInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存商家
     */
    @RequiresPermissions("app:merchantInfo:edit")
    @Log(title = "商家", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MerchantInfo merchantInfo)
    {
        return toAjax(merchantInfoService.updateMerchantInfo(merchantInfo));
    }

    /**
     * 删除商家
     */
    @RequiresPermissions("app:merchantInfo:remove")
    @Log(title = "商家", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(merchantInfoService.deleteMerchantInfoByIds(ids));
    }
}
