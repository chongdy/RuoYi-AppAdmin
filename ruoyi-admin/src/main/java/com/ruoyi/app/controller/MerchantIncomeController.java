package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MerchantIncome;
import com.ruoyi.app.service.IMerchantIncomeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 推荐商户收入Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/merchantIncome")
public class MerchantIncomeController extends BaseController
{
    private String prefix = "app/merchantIncome";

    @Autowired
    private IMerchantIncomeService merchantIncomeService;

    @RequiresPermissions("app:merchantIncome:view")
    @GetMapping()
    public String merchantIncome()
    {
        return prefix + "/merchantIncome";
    }

    /**
     * 查询推荐商户收入列表
     */
    @RequiresPermissions("app:merchantIncome:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MerchantIncome merchantIncome)
    {
        startPage();
        List<MerchantIncome> list = merchantIncomeService.selectMerchantIncomeList(merchantIncome);
        return getDataTable(list);
    }

    /**
     * 导出推荐商户收入列表
     */
    @RequiresPermissions("app:merchantIncome:export")
    @Log(title = "推荐商户收入", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MerchantIncome merchantIncome)
    {
        List<MerchantIncome> list = merchantIncomeService.selectMerchantIncomeList(merchantIncome);
        ExcelUtil<MerchantIncome> util = new ExcelUtil<MerchantIncome>(MerchantIncome.class);
        return util.exportExcel(list, "merchantIncome");
    }

    /**
     * 新增推荐商户收入
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存推荐商户收入
     */
    @RequiresPermissions("app:merchantIncome:add")
    @Log(title = "推荐商户收入", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MerchantIncome merchantIncome)
    {
        return toAjax(merchantIncomeService.insertMerchantIncome(merchantIncome));
    }

    /**
     * 修改推荐商户收入
     */
    @GetMapping("/edit/{merIncomeId}")
    public String edit(@PathVariable("merIncomeId") String merIncomeId, ModelMap mmap)
    {
        MerchantIncome merchantIncome = merchantIncomeService.selectMerchantIncomeById(merIncomeId);
        mmap.put("merchantIncome", merchantIncome);
        return prefix + "/edit";
    }

    /**
     * 修改保存推荐商户收入
     */
    @RequiresPermissions("app:merchantIncome:edit")
    @Log(title = "推荐商户收入", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MerchantIncome merchantIncome)
    {
        return toAjax(merchantIncomeService.updateMerchantIncome(merchantIncome));
    }

    /**
     * 删除推荐商户收入
     */
    @RequiresPermissions("app:merchantIncome:remove")
    @Log(title = "推荐商户收入", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(merchantIncomeService.deleteMerchantIncomeByIds(ids));
    }
}
