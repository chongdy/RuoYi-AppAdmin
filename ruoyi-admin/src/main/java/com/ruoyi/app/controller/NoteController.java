package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.Note;
import com.ruoyi.app.service.INoteService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游记Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/note")
public class NoteController extends BaseController
{
    private String prefix = "app/note";

    @Autowired
    private INoteService noteService;

    @RequiresPermissions("app:note:view")
    @GetMapping()
    public String note()
    {
        return prefix + "/note";
    }

    /**
     * 查询游记列表
     */
    @RequiresPermissions("app:note:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Note note)
    {
        startPage();
        List<Note> list = noteService.selectNoteList(note);
        return getDataTable(list);
    }

    /**
     * 导出游记列表
     */
    @RequiresPermissions("app:note:export")
    @Log(title = "游记", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Note note)
    {
        List<Note> list = noteService.selectNoteList(note);
        ExcelUtil<Note> util = new ExcelUtil<Note>(Note.class);
        return util.exportExcel(list, "note");
    }

    /**
     * 新增游记
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存游记
     */
    @RequiresPermissions("app:note:add")
    @Log(title = "游记", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Note note)
    {
        return toAjax(noteService.insertNote(note));
    }

    /**
     * 修改游记
     */
    @GetMapping("/edit/{noteId}")
    public String edit(@PathVariable("noteId") String noteId, ModelMap mmap)
    {
        Note note = noteService.selectNoteById(noteId);
        mmap.put("note", note);
        return prefix + "/edit";
    }

    /**
     * 修改保存游记
     */
    @RequiresPermissions("app:note:edit")
    @Log(title = "游记", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Note note)
    {
        return toAjax(noteService.updateNote(note));
    }

    /**
     * 删除游记
     */
    @RequiresPermissions("app:note:remove")
    @Log(title = "游记", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(noteService.deleteNoteByIds(ids));
    }
}
