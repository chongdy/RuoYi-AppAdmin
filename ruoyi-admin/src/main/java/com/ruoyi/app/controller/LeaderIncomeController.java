package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.LeaderIncome;
import com.ruoyi.app.service.ILeaderIncomeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 领队收入Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/leaderIncome")
public class LeaderIncomeController extends BaseController
{
    private String prefix = "app/leaderIncome";

    @Autowired
    private ILeaderIncomeService leaderIncomeService;

    @RequiresPermissions("app:leaderIncome:view")
    @GetMapping()
    public String leaderIncome()
    {
        return prefix + "/leaderIncome";
    }

    /**
     * 查询领队收入列表
     */
    @RequiresPermissions("app:leaderIncome:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LeaderIncome leaderIncome)
    {
        startPage();
        List<LeaderIncome> list = leaderIncomeService.selectLeaderIncomeList(leaderIncome);
        return getDataTable(list);
    }

    /**
     * 导出领队收入列表
     */
    @RequiresPermissions("app:leaderIncome:export")
    @Log(title = "领队收入", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LeaderIncome leaderIncome)
    {
        List<LeaderIncome> list = leaderIncomeService.selectLeaderIncomeList(leaderIncome);
        ExcelUtil<LeaderIncome> util = new ExcelUtil<LeaderIncome>(LeaderIncome.class);
        return util.exportExcel(list, "leaderIncome");
    }

    /**
     * 新增领队收入
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存领队收入
     */
    @RequiresPermissions("app:leaderIncome:add")
    @Log(title = "领队收入", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(LeaderIncome leaderIncome)
    {
        return toAjax(leaderIncomeService.insertLeaderIncome(leaderIncome));
    }

    /**
     * 修改领队收入
     */
    @GetMapping("/edit/{leaderIncomeId}")
    public String edit(@PathVariable("leaderIncomeId") String leaderIncomeId, ModelMap mmap)
    {
        LeaderIncome leaderIncome = leaderIncomeService.selectLeaderIncomeById(leaderIncomeId);
        mmap.put("leaderIncome", leaderIncome);
        return prefix + "/edit";
    }

    /**
     * 修改保存领队收入
     */
    @RequiresPermissions("app:leaderIncome:edit")
    @Log(title = "领队收入", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LeaderIncome leaderIncome)
    {
        return toAjax(leaderIncomeService.updateLeaderIncome(leaderIncome));
    }

    /**
     * 删除领队收入
     */
    @RequiresPermissions("app:leaderIncome:remove")
    @Log(title = "领队收入", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(leaderIncomeService.deleteLeaderIncomeByIds(ids));
    }
}
