package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.GroupType;
import com.ruoyi.app.service.IGroupTypeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 群类型Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/groupType")
public class GroupTypeController extends BaseController
{
    private String prefix = "app/groupType";

    @Autowired
    private IGroupTypeService groupTypeService;

    @RequiresPermissions("app:groupType:view")
    @GetMapping()
    public String groupType()
    {
        return prefix + "/groupType";
    }

    /**
     * 查询群类型列表
     */
    @RequiresPermissions("app:groupType:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GroupType groupType)
    {
        startPage();
        List<GroupType> list = groupTypeService.selectGroupTypeList(groupType);
        return getDataTable(list);
    }

    /**
     * 导出群类型列表
     */
    @RequiresPermissions("app:groupType:export")
    @Log(title = "群类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GroupType groupType)
    {
        List<GroupType> list = groupTypeService.selectGroupTypeList(groupType);
        ExcelUtil<GroupType> util = new ExcelUtil<GroupType>(GroupType.class);
        return util.exportExcel(list, "groupType");
    }

    /**
     * 新增群类型
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存群类型
     */
    @RequiresPermissions("app:groupType:add")
    @Log(title = "群类型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GroupType groupType)
    {
        return toAjax(groupTypeService.insertGroupType(groupType));
    }

    /**
     * 修改群类型
     */
    @GetMapping("/edit/{groupTypeId}")
    public String edit(@PathVariable("groupTypeId") Long groupTypeId, ModelMap mmap)
    {
        GroupType groupType = groupTypeService.selectGroupTypeById(groupTypeId);
        mmap.put("groupType", groupType);
        return prefix + "/edit";
    }

    /**
     * 修改保存群类型
     */
    @RequiresPermissions("app:groupType:edit")
    @Log(title = "群类型", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GroupType groupType)
    {
        return toAjax(groupTypeService.updateGroupType(groupType));
    }

    /**
     * 删除群类型
     */
    @RequiresPermissions("app:groupType:remove")
    @Log(title = "群类型", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(groupTypeService.deleteGroupTypeByIds(ids));
    }
}
