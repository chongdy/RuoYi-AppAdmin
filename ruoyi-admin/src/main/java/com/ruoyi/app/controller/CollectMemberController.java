package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.CollectMember;
import com.ruoyi.app.service.ICollectMemberService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 收藏会员Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/collectMember")
public class CollectMemberController extends BaseController
{
    private String prefix = "app/collectMember";

    @Autowired
    private ICollectMemberService collectMemberService;

    @RequiresPermissions("app:collectMember:view")
    @GetMapping()
    public String collectMember()
    {
        return prefix + "/collectMember";
    }

    /**
     * 查询收藏会员列表
     */
    @RequiresPermissions("app:collectMember:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CollectMember collectMember)
    {
        startPage();
        List<CollectMember> list = collectMemberService.selectCollectMemberList(collectMember);
        return getDataTable(list);
    }

    /**
     * 导出收藏会员列表
     */
    @RequiresPermissions("app:collectMember:export")
    @Log(title = "收藏会员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CollectMember collectMember)
    {
        List<CollectMember> list = collectMemberService.selectCollectMemberList(collectMember);
        ExcelUtil<CollectMember> util = new ExcelUtil<CollectMember>(CollectMember.class);
        return util.exportExcel(list, "collectMember");
    }

    /**
     * 新增收藏会员
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存收藏会员
     */
    @RequiresPermissions("app:collectMember:add")
    @Log(title = "收藏会员", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CollectMember collectMember)
    {
        return toAjax(collectMemberService.insertCollectMember(collectMember));
    }

    /**
     * 修改收藏会员
     */
    @GetMapping("/edit/{collectId}")
    public String edit(@PathVariable("collectId") Long collectId, ModelMap mmap)
    {
        CollectMember collectMember = collectMemberService.selectCollectMemberById(collectId);
        mmap.put("collectMember", collectMember);
        return prefix + "/edit";
    }

    /**
     * 修改保存收藏会员
     */
    @RequiresPermissions("app:collectMember:edit")
    @Log(title = "收藏会员", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CollectMember collectMember)
    {
        return toAjax(collectMemberService.updateCollectMember(collectMember));
    }

    /**
     * 删除收藏会员
     */
    @RequiresPermissions("app:collectMember:remove")
    @Log(title = "收藏会员", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(collectMemberService.deleteCollectMemberByIds(ids));
    }
}
