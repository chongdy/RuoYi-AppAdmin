package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.PlazaNofeelTag;
import com.ruoyi.app.service.IPlazaNofeelTagService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 广场不感兴趣Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/plazaNofeelTag")
public class PlazaNofeelTagController extends BaseController
{
    private String prefix = "app/plazaNofeelTag";

    @Autowired
    private IPlazaNofeelTagService plazaNofeelTagService;

    @RequiresPermissions("app:plazaNofeelTag:view")
    @GetMapping()
    public String plazaNofeelTag()
    {
        return prefix + "/plazaNofeelTag";
    }

    /**
     * 查询广场不感兴趣列表
     */
    @RequiresPermissions("app:plazaNofeelTag:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PlazaNofeelTag plazaNofeelTag)
    {
        startPage();
        List<PlazaNofeelTag> list = plazaNofeelTagService.selectPlazaNofeelTagList(plazaNofeelTag);
        return getDataTable(list);
    }

    /**
     * 导出广场不感兴趣列表
     */
    @RequiresPermissions("app:plazaNofeelTag:export")
    @Log(title = "广场不感兴趣", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlazaNofeelTag plazaNofeelTag)
    {
        List<PlazaNofeelTag> list = plazaNofeelTagService.selectPlazaNofeelTagList(plazaNofeelTag);
        ExcelUtil<PlazaNofeelTag> util = new ExcelUtil<PlazaNofeelTag>(PlazaNofeelTag.class);
        return util.exportExcel(list, "plazaNofeelTag");
    }

    /**
     * 新增广场不感兴趣
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存广场不感兴趣
     */
    @RequiresPermissions("app:plazaNofeelTag:add")
    @Log(title = "广场不感兴趣", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PlazaNofeelTag plazaNofeelTag)
    {
        return toAjax(plazaNofeelTagService.insertPlazaNofeelTag(plazaNofeelTag));
    }

    /**
     * 修改广场不感兴趣
     */
    @GetMapping("/edit/{nofeelId}")
    public String edit(@PathVariable("nofeelId") String nofeelId, ModelMap mmap)
    {
        PlazaNofeelTag plazaNofeelTag = plazaNofeelTagService.selectPlazaNofeelTagById(nofeelId);
        mmap.put("plazaNofeelTag", plazaNofeelTag);
        return prefix + "/edit";
    }

    /**
     * 修改保存广场不感兴趣
     */
    @RequiresPermissions("app:plazaNofeelTag:edit")
    @Log(title = "广场不感兴趣", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlazaNofeelTag plazaNofeelTag)
    {
        return toAjax(plazaNofeelTagService.updatePlazaNofeelTag(plazaNofeelTag));
    }

    /**
     * 删除广场不感兴趣
     */
    @RequiresPermissions("app:plazaNofeelTag:remove")
    @Log(title = "广场不感兴趣", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(plazaNofeelTagService.deletePlazaNofeelTagByIds(ids));
    }
}
