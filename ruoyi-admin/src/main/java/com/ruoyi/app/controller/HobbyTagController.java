package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.HobbyTag;
import com.ruoyi.app.service.IHobbyTagService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 兴趣标签Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/hobbyTag")
public class HobbyTagController extends BaseController
{
    private String prefix = "app/hobbyTag";

    @Autowired
    private IHobbyTagService hobbyTagService;

    @RequiresPermissions("app:hobbyTag:view")
    @GetMapping()
    public String hobbyTag()
    {
        return prefix + "/hobbyTag";
    }

    /**
     * 查询兴趣标签列表
     */
    @RequiresPermissions("app:hobbyTag:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HobbyTag hobbyTag)
    {
        startPage();
        List<HobbyTag> list = hobbyTagService.selectHobbyTagList(hobbyTag);
        return getDataTable(list);
    }

    /**
     * 导出兴趣标签列表
     */
    @RequiresPermissions("app:hobbyTag:export")
    @Log(title = "兴趣标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HobbyTag hobbyTag)
    {
        List<HobbyTag> list = hobbyTagService.selectHobbyTagList(hobbyTag);
        ExcelUtil<HobbyTag> util = new ExcelUtil<HobbyTag>(HobbyTag.class);
        return util.exportExcel(list, "hobbyTag");
    }

    /**
     * 新增兴趣标签
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存兴趣标签
     */
    @RequiresPermissions("app:hobbyTag:add")
    @Log(title = "兴趣标签", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HobbyTag hobbyTag)
    {
        return toAjax(hobbyTagService.insertHobbyTag(hobbyTag));
    }

    /**
     * 修改兴趣标签
     */
    @GetMapping("/edit/{tagId}")
    public String edit(@PathVariable("tagId") String tagId, ModelMap mmap)
    {
        HobbyTag hobbyTag = hobbyTagService.selectHobbyTagById(tagId);
        mmap.put("hobbyTag", hobbyTag);
        return prefix + "/edit";
    }

    /**
     * 修改保存兴趣标签
     */
    @RequiresPermissions("app:hobbyTag:edit")
    @Log(title = "兴趣标签", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HobbyTag hobbyTag)
    {
        return toAjax(hobbyTagService.updateHobbyTag(hobbyTag));
    }

    /**
     * 删除兴趣标签
     */
    @RequiresPermissions("app:hobbyTag:remove")
    @Log(title = "兴趣标签", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hobbyTagService.deleteHobbyTagByIds(ids));
    }
}
