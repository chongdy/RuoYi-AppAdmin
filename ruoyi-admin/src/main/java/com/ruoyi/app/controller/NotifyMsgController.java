package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.NotifyMsg;
import com.ruoyi.app.service.INotifyMsgService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 通知Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/notifyMsg")
public class NotifyMsgController extends BaseController
{
    private String prefix = "app/notifyMsg";

    @Autowired
    private INotifyMsgService notifyMsgService;

    @RequiresPermissions("app:notifyMsg:view")
    @GetMapping()
    public String notifyMsg()
    {
        return prefix + "/notifyMsg";
    }

    /**
     * 查询通知列表
     */
    @RequiresPermissions("app:notifyMsg:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NotifyMsg notifyMsg)
    {
        startPage();
        List<NotifyMsg> list = notifyMsgService.selectNotifyMsgList(notifyMsg);
        return getDataTable(list);
    }

    /**
     * 导出通知列表
     */
    @RequiresPermissions("app:notifyMsg:export")
    @Log(title = "通知", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NotifyMsg notifyMsg)
    {
        List<NotifyMsg> list = notifyMsgService.selectNotifyMsgList(notifyMsg);
        ExcelUtil<NotifyMsg> util = new ExcelUtil<NotifyMsg>(NotifyMsg.class);
        return util.exportExcel(list, "notifyMsg");
    }

    /**
     * 新增通知
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存通知
     */
    @RequiresPermissions("app:notifyMsg:add")
    @Log(title = "通知", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NotifyMsg notifyMsg)
    {
        return toAjax(notifyMsgService.insertNotifyMsg(notifyMsg));
    }

    /**
     * 修改通知
     */
    @GetMapping("/edit/{notifyId}")
    public String edit(@PathVariable("notifyId") String notifyId, ModelMap mmap)
    {
        NotifyMsg notifyMsg = notifyMsgService.selectNotifyMsgById(notifyId);
        mmap.put("notifyMsg", notifyMsg);
        return prefix + "/edit";
    }

    /**
     * 修改保存通知
     */
    @RequiresPermissions("app:notifyMsg:edit")
    @Log(title = "通知", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NotifyMsg notifyMsg)
    {
        return toAjax(notifyMsgService.updateNotifyMsg(notifyMsg));
    }

    /**
     * 删除通知
     */
    @RequiresPermissions("app:notifyMsg:remove")
    @Log(title = "通知", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(notifyMsgService.deleteNotifyMsgByIds(ids));
    }
}
