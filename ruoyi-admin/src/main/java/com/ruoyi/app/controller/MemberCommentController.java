package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MemberComment;
import com.ruoyi.app.service.IMemberCommentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员评价Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/memberComment")
public class MemberCommentController extends BaseController
{
    private String prefix = "app/memberComment";

    @Autowired
    private IMemberCommentService memberCommentService;

    @RequiresPermissions("app:memberComment:view")
    @GetMapping()
    public String memberComment()
    {
        return prefix + "/memberComment";
    }

    /**
     * 查询会员评价列表
     */
    @RequiresPermissions("app:memberComment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberComment memberComment)
    {
        startPage();
        List<MemberComment> list = memberCommentService.selectMemberCommentList(memberComment);
        return getDataTable(list);
    }

    /**
     * 导出会员评价列表
     */
    @RequiresPermissions("app:memberComment:export")
    @Log(title = "会员评价", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberComment memberComment)
    {
        List<MemberComment> list = memberCommentService.selectMemberCommentList(memberComment);
        ExcelUtil<MemberComment> util = new ExcelUtil<MemberComment>(MemberComment.class);
        return util.exportExcel(list, "memberComment");
    }

    /**
     * 新增会员评价
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存会员评价
     */
    @RequiresPermissions("app:memberComment:add")
    @Log(title = "会员评价", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberComment memberComment)
    {
        return toAjax(memberCommentService.insertMemberComment(memberComment));
    }

    /**
     * 修改会员评价
     */
    @GetMapping("/edit/{memCommId}")
    public String edit(@PathVariable("memCommId") Long memCommId, ModelMap mmap)
    {
        MemberComment memberComment = memberCommentService.selectMemberCommentById(memCommId);
        mmap.put("memberComment", memberComment);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员评价
     */
    @RequiresPermissions("app:memberComment:edit")
    @Log(title = "会员评价", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberComment memberComment)
    {
        return toAjax(memberCommentService.updateMemberComment(memberComment));
    }

    /**
     * 删除会员评价
     */
    @RequiresPermissions("app:memberComment:remove")
    @Log(title = "会员评价", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberCommentService.deleteMemberCommentByIds(ids));
    }
}
