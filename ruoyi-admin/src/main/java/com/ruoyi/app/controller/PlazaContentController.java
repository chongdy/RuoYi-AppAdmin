package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.PlazaContent;
import com.ruoyi.app.service.IPlazaContentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 广场动态Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/plazaContent")
public class PlazaContentController extends BaseController
{
    private String prefix = "app/plazaContent";

    @Autowired
    private IPlazaContentService plazaContentService;

    @RequiresPermissions("app:plazaContent:view")
    @GetMapping()
    public String plazaContent()
    {
        return prefix + "/plazaContent";
    }

    /**
     * 查询广场动态列表
     */
    @RequiresPermissions("app:plazaContent:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PlazaContent plazaContent)
    {
        startPage();
        List<PlazaContent> list = plazaContentService.selectPlazaContentList(plazaContent);
        return getDataTable(list);
    }

    /**
     * 导出广场动态列表
     */
    @RequiresPermissions("app:plazaContent:export")
    @Log(title = "广场动态", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlazaContent plazaContent)
    {
        List<PlazaContent> list = plazaContentService.selectPlazaContentList(plazaContent);
        ExcelUtil<PlazaContent> util = new ExcelUtil<PlazaContent>(PlazaContent.class);
        return util.exportExcel(list, "plazaContent");
    }

    /**
     * 新增广场动态
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存广场动态
     */
    @RequiresPermissions("app:plazaContent:add")
    @Log(title = "广场动态", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PlazaContent plazaContent)
    {
        return toAjax(plazaContentService.insertPlazaContent(plazaContent));
    }

    /**
     * 修改广场动态
     */
    @GetMapping("/edit/{contentId}")
    public String edit(@PathVariable("contentId") String contentId, ModelMap mmap)
    {
        PlazaContent plazaContent = plazaContentService.selectPlazaContentById(contentId);
        mmap.put("plazaContent", plazaContent);
        return prefix + "/edit";
    }

    /**
     * 修改保存广场动态
     */
    @RequiresPermissions("app:plazaContent:edit")
    @Log(title = "广场动态", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlazaContent plazaContent)
    {
        return toAjax(plazaContentService.updatePlazaContent(plazaContent));
    }

    /**
     * 删除广场动态
     */
    @RequiresPermissions("app:plazaContent:remove")
    @Log(title = "广场动态", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(plazaContentService.deletePlazaContentByIds(ids));
    }
}
