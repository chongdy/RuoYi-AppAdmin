package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.NoteIncome;
import com.ruoyi.app.service.INoteIncomeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游记收入Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/noteIncome")
public class NoteIncomeController extends BaseController
{
    private String prefix = "app/noteIncome";

    @Autowired
    private INoteIncomeService noteIncomeService;

    @RequiresPermissions("app:noteIncome:view")
    @GetMapping()
    public String noteIncome()
    {
        return prefix + "/noteIncome";
    }

    /**
     * 查询游记收入列表
     */
    @RequiresPermissions("app:noteIncome:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NoteIncome noteIncome)
    {
        startPage();
        List<NoteIncome> list = noteIncomeService.selectNoteIncomeList(noteIncome);
        return getDataTable(list);
    }

    /**
     * 导出游记收入列表
     */
    @RequiresPermissions("app:noteIncome:export")
    @Log(title = "游记收入", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NoteIncome noteIncome)
    {
        List<NoteIncome> list = noteIncomeService.selectNoteIncomeList(noteIncome);
        ExcelUtil<NoteIncome> util = new ExcelUtil<NoteIncome>(NoteIncome.class);
        return util.exportExcel(list, "noteIncome");
    }

    /**
     * 新增游记收入
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存游记收入
     */
    @RequiresPermissions("app:noteIncome:add")
    @Log(title = "游记收入", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NoteIncome noteIncome)
    {
        return toAjax(noteIncomeService.insertNoteIncome(noteIncome));
    }

    /**
     * 修改游记收入
     */
    @GetMapping("/edit/{noteIncomeId}")
    public String edit(@PathVariable("noteIncomeId") String noteIncomeId, ModelMap mmap)
    {
        NoteIncome noteIncome = noteIncomeService.selectNoteIncomeById(noteIncomeId);
        mmap.put("noteIncome", noteIncome);
        return prefix + "/edit";
    }

    /**
     * 修改保存游记收入
     */
    @RequiresPermissions("app:noteIncome:edit")
    @Log(title = "游记收入", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NoteIncome noteIncome)
    {
        return toAjax(noteIncomeService.updateNoteIncome(noteIncome));
    }

    /**
     * 删除游记收入
     */
    @RequiresPermissions("app:noteIncome:remove")
    @Log(title = "游记收入", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(noteIncomeService.deleteNoteIncomeByIds(ids));
    }
}
