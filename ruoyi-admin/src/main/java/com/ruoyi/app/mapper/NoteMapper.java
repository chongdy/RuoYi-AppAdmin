package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.Note;
import java.util.List;

/**
 * 游记Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface NoteMapper 
{
    /**
     * 查询游记
     * 
     * @param noteId 游记ID
     * @return 游记
     */
    public Note selectNoteById(String noteId);

    /**
     * 查询游记列表
     * 
     * @param note 游记
     * @return 游记集合
     */
    public List<Note> selectNoteList(Note note);

    /**
     * 新增游记
     * 
     * @param note 游记
     * @return 结果
     */
    public int insertNote(Note note);

    /**
     * 修改游记
     * 
     * @param note 游记
     * @return 结果
     */
    public int updateNote(Note note);

    /**
     * 删除游记
     * 
     * @param noteId 游记ID
     * @return 结果
     */
    public int deleteNoteById(String noteId);

    /**
     * 批量删除游记
     * 
     * @param noteIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteNoteByIds(String[] noteIds);
}
