package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.RecommentAct;
import java.util.List;

/**
 * 推荐活动Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface RecommentActMapper 
{
    /**
     * 查询推荐活动
     * 
     * @param recId 推荐活动ID
     * @return 推荐活动
     */
    public RecommentAct selectRecommentActById(String recId);

    /**
     * 查询推荐活动列表
     * 
     * @param recommentAct 推荐活动
     * @return 推荐活动集合
     */
    public List<RecommentAct> selectRecommentActList(RecommentAct recommentAct);

    /**
     * 新增推荐活动
     * 
     * @param recommentAct 推荐活动
     * @return 结果
     */
    public int insertRecommentAct(RecommentAct recommentAct);

    /**
     * 修改推荐活动
     * 
     * @param recommentAct 推荐活动
     * @return 结果
     */
    public int updateRecommentAct(RecommentAct recommentAct);

    /**
     * 删除推荐活动
     * 
     * @param recId 推荐活动ID
     * @return 结果
     */
    public int deleteRecommentActById(String recId);

    /**
     * 批量删除推荐活动
     * 
     * @param recIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecommentActByIds(String[] recIds);
}
