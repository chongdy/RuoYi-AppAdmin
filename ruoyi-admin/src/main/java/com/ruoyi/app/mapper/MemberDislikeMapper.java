package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.MemberDislike;
import java.util.List;

/**
 * 不喜欢Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface MemberDislikeMapper 
{
    /**
     * 查询不喜欢
     * 
     * @param dislikeId 不喜欢ID
     * @return 不喜欢
     */
    public MemberDislike selectMemberDislikeById(Long dislikeId);

    /**
     * 查询不喜欢列表
     * 
     * @param memberDislike 不喜欢
     * @return 不喜欢集合
     */
    public List<MemberDislike> selectMemberDislikeList(MemberDislike memberDislike);

    /**
     * 新增不喜欢
     * 
     * @param memberDislike 不喜欢
     * @return 结果
     */
    public int insertMemberDislike(MemberDislike memberDislike);

    /**
     * 修改不喜欢
     * 
     * @param memberDislike 不喜欢
     * @return 结果
     */
    public int updateMemberDislike(MemberDislike memberDislike);

    /**
     * 删除不喜欢
     * 
     * @param dislikeId 不喜欢ID
     * @return 结果
     */
    public int deleteMemberDislikeById(Long dislikeId);

    /**
     * 批量删除不喜欢
     * 
     * @param dislikeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberDislikeByIds(String[] dislikeIds);
}
