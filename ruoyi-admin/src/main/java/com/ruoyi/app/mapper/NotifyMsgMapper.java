package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.NotifyMsg;
import java.util.List;

/**
 * 通知Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface NotifyMsgMapper 
{
    /**
     * 查询通知
     * 
     * @param notifyId 通知ID
     * @return 通知
     */
    public NotifyMsg selectNotifyMsgById(String notifyId);

    /**
     * 查询通知列表
     * 
     * @param notifyMsg 通知
     * @return 通知集合
     */
    public List<NotifyMsg> selectNotifyMsgList(NotifyMsg notifyMsg);

    /**
     * 新增通知
     * 
     * @param notifyMsg 通知
     * @return 结果
     */
    public int insertNotifyMsg(NotifyMsg notifyMsg);

    /**
     * 修改通知
     * 
     * @param notifyMsg 通知
     * @return 结果
     */
    public int updateNotifyMsg(NotifyMsg notifyMsg);

    /**
     * 删除通知
     * 
     * @param notifyId 通知ID
     * @return 结果
     */
    public int deleteNotifyMsgById(String notifyId);

    /**
     * 批量删除通知
     * 
     * @param notifyIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteNotifyMsgByIds(String[] notifyIds);
}
