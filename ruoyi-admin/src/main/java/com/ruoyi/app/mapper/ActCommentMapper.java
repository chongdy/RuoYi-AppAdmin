package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.ActComment;
import java.util.List;

/**
 * 活动评价Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface ActCommentMapper 
{
    /**
     * 查询活动评价
     * 
     * @param actCommId 活动评价ID
     * @return 活动评价
     */
    public ActComment selectActCommentById(Long actCommId);

    /**
     * 查询活动评价列表
     * 
     * @param actComment 活动评价
     * @return 活动评价集合
     */
    public List<ActComment> selectActCommentList(ActComment actComment);

    /**
     * 新增活动评价
     * 
     * @param actComment 活动评价
     * @return 结果
     */
    public int insertActComment(ActComment actComment);

    /**
     * 修改活动评价
     * 
     * @param actComment 活动评价
     * @return 结果
     */
    public int updateActComment(ActComment actComment);

    /**
     * 删除活动评价
     * 
     * @param actCommId 活动评价ID
     * @return 结果
     */
    public int deleteActCommentById(Long actCommId);

    /**
     * 批量删除活动评价
     * 
     * @param actCommIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteActCommentByIds(String[] actCommIds);
}
