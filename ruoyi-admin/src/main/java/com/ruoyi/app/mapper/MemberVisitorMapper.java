package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.MemberVisitor;
import java.util.List;

/**
 * 访客Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface MemberVisitorMapper 
{
    /**
     * 查询访客
     * 
     * @param visitorId 访客ID
     * @return 访客
     */
    public MemberVisitor selectMemberVisitorById(Long visitorId);

    /**
     * 查询访客列表
     * 
     * @param memberVisitor 访客
     * @return 访客集合
     */
    public List<MemberVisitor> selectMemberVisitorList(MemberVisitor memberVisitor);

    /**
     * 新增访客
     * 
     * @param memberVisitor 访客
     * @return 结果
     */
    public int insertMemberVisitor(MemberVisitor memberVisitor);

    /**
     * 修改访客
     * 
     * @param memberVisitor 访客
     * @return 结果
     */
    public int updateMemberVisitor(MemberVisitor memberVisitor);

    /**
     * 删除访客
     * 
     * @param visitorId 访客ID
     * @return 结果
     */
    public int deleteMemberVisitorById(Long visitorId);

    /**
     * 批量删除访客
     * 
     * @param visitorIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberVisitorByIds(String[] visitorIds);
}
