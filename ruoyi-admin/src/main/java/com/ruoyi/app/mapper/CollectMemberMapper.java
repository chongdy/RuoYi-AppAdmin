package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.CollectMember;
import java.util.List;

/**
 * 收藏会员Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface CollectMemberMapper 
{
    /**
     * 查询收藏会员
     * 
     * @param collectId 收藏会员ID
     * @return 收藏会员
     */
    public CollectMember selectCollectMemberById(Long collectId);

    /**
     * 查询收藏会员列表
     * 
     * @param collectMember 收藏会员
     * @return 收藏会员集合
     */
    public List<CollectMember> selectCollectMemberList(CollectMember collectMember);

    /**
     * 新增收藏会员
     * 
     * @param collectMember 收藏会员
     * @return 结果
     */
    public int insertCollectMember(CollectMember collectMember);

    /**
     * 修改收藏会员
     * 
     * @param collectMember 收藏会员
     * @return 结果
     */
    public int updateCollectMember(CollectMember collectMember);

    /**
     * 删除收藏会员
     * 
     * @param collectId 收藏会员ID
     * @return 结果
     */
    public int deleteCollectMemberById(Long collectId);

    /**
     * 批量删除收藏会员
     * 
     * @param collectIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteCollectMemberByIds(String[] collectIds);
}
