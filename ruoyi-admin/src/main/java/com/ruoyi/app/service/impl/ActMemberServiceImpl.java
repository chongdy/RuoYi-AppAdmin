package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.ActMemberMapper;
import com.ruoyi.app.domain.ActMember;
import com.ruoyi.app.service.IActMemberService;
import com.ruoyi.common.core.text.Convert;

/**
 * 报名记录Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class ActMemberServiceImpl implements IActMemberService 
{
    @Autowired
    private ActMemberMapper actMemberMapper;

    /**
     * 查询报名记录
     * 
     * @param acmId 报名记录ID
     * @return 报名记录
     */
    @Override
    public ActMember selectActMemberById(String acmId)
    {
        return actMemberMapper.selectActMemberById(acmId);
    }

    /**
     * 查询报名记录列表
     * 
     * @param actMember 报名记录
     * @return 报名记录
     */
    @Override
    public List<ActMember> selectActMemberList(ActMember actMember)
    {
        return actMemberMapper.selectActMemberList(actMember);
    }

    /**
     * 新增报名记录
     * 
     * @param actMember 报名记录
     * @return 结果
     */
    @Override
    public int insertActMember(ActMember actMember)
    {
        actMember.setCreateTime(DateUtils.getNowDate());
        return actMemberMapper.insertActMember(actMember);
    }

    /**
     * 修改报名记录
     * 
     * @param actMember 报名记录
     * @return 结果
     */
    @Override
    public int updateActMember(ActMember actMember)
    {
        return actMemberMapper.updateActMember(actMember);
    }

    /**
     * 删除报名记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActMemberByIds(String ids)
    {
        return actMemberMapper.deleteActMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除报名记录信息
     * 
     * @param acmId 报名记录ID
     * @return 结果
     */
    @Override
    public int deleteActMemberById(String acmId)
    {
        return actMemberMapper.deleteActMemberById(acmId);
    }
}
