package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MerchantTypeMapper;
import com.ruoyi.app.domain.MerchantType;
import com.ruoyi.app.service.IMerchantTypeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商家分类Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MerchantTypeServiceImpl implements IMerchantTypeService 
{
    @Autowired
    private MerchantTypeMapper merchantTypeMapper;

    /**
     * 查询商家分类
     * 
     * @param merTypeId 商家分类ID
     * @return 商家分类
     */
    @Override
    public MerchantType selectMerchantTypeById(Long merTypeId)
    {
        return merchantTypeMapper.selectMerchantTypeById(merTypeId);
    }

    /**
     * 查询商家分类列表
     * 
     * @param merchantType 商家分类
     * @return 商家分类
     */
    @Override
    public List<MerchantType> selectMerchantTypeList(MerchantType merchantType)
    {
        return merchantTypeMapper.selectMerchantTypeList(merchantType);
    }

    /**
     * 新增商家分类
     * 
     * @param merchantType 商家分类
     * @return 结果
     */
    @Override
    public int insertMerchantType(MerchantType merchantType)
    {
        merchantType.setCreateTime(DateUtils.getNowDate());
        return merchantTypeMapper.insertMerchantType(merchantType);
    }

    /**
     * 修改商家分类
     * 
     * @param merchantType 商家分类
     * @return 结果
     */
    @Override
    public int updateMerchantType(MerchantType merchantType)
    {
        return merchantTypeMapper.updateMerchantType(merchantType);
    }

    /**
     * 删除商家分类对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMerchantTypeByIds(String ids)
    {
        return merchantTypeMapper.deleteMerchantTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商家分类信息
     * 
     * @param merTypeId 商家分类ID
     * @return 结果
     */
    @Override
    public int deleteMerchantTypeById(Long merTypeId)
    {
        return merchantTypeMapper.deleteMerchantTypeById(merTypeId);
    }
}
