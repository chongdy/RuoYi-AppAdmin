package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MemberCommentMapper;
import com.ruoyi.app.domain.MemberComment;
import com.ruoyi.app.service.IMemberCommentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 会员评价Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MemberCommentServiceImpl implements IMemberCommentService 
{
    @Autowired
    private MemberCommentMapper memberCommentMapper;

    /**
     * 查询会员评价
     * 
     * @param memCommId 会员评价ID
     * @return 会员评价
     */
    @Override
    public MemberComment selectMemberCommentById(Long memCommId)
    {
        return memberCommentMapper.selectMemberCommentById(memCommId);
    }

    /**
     * 查询会员评价列表
     * 
     * @param memberComment 会员评价
     * @return 会员评价
     */
    @Override
    public List<MemberComment> selectMemberCommentList(MemberComment memberComment)
    {
        return memberCommentMapper.selectMemberCommentList(memberComment);
    }

    /**
     * 新增会员评价
     * 
     * @param memberComment 会员评价
     * @return 结果
     */
    @Override
    public int insertMemberComment(MemberComment memberComment)
    {
        memberComment.setCreateTime(DateUtils.getNowDate());
        return memberCommentMapper.insertMemberComment(memberComment);
    }

    /**
     * 修改会员评价
     * 
     * @param memberComment 会员评价
     * @return 结果
     */
    @Override
    public int updateMemberComment(MemberComment memberComment)
    {
        return memberCommentMapper.updateMemberComment(memberComment);
    }

    /**
     * 删除会员评价对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberCommentByIds(String ids)
    {
        return memberCommentMapper.deleteMemberCommentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员评价信息
     * 
     * @param memCommId 会员评价ID
     * @return 结果
     */
    @Override
    public int deleteMemberCommentById(Long memCommId)
    {
        return memberCommentMapper.deleteMemberCommentById(memCommId);
    }
}
