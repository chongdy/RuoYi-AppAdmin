package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.GroupMapper;
import com.ruoyi.app.domain.Group;
import com.ruoyi.app.service.IGroupService;
import com.ruoyi.common.core.text.Convert;

/**
 * 微信群Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class GroupServiceImpl implements IGroupService 
{
    @Autowired
    private GroupMapper groupMapper;

    /**
     * 查询微信群
     * 
     * @param groupId 微信群ID
     * @return 微信群
     */
    @Override
    public Group selectGroupById(String groupId)
    {
        return groupMapper.selectGroupById(groupId);
    }

    /**
     * 查询微信群列表
     * 
     * @param group 微信群
     * @return 微信群
     */
    @Override
    public List<Group> selectGroupList(Group group)
    {
        return groupMapper.selectGroupList(group);
    }

    /**
     * 新增微信群
     * 
     * @param group 微信群
     * @return 结果
     */
    @Override
    public int insertGroup(Group group)
    {
        group.setCreateTime(DateUtils.getNowDate());
        return groupMapper.insertGroup(group);
    }

    /**
     * 修改微信群
     * 
     * @param group 微信群
     * @return 结果
     */
    @Override
    public int updateGroup(Group group)
    {
        return groupMapper.updateGroup(group);
    }

    /**
     * 删除微信群对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGroupByIds(String ids)
    {
        return groupMapper.deleteGroupByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除微信群信息
     * 
     * @param groupId 微信群ID
     * @return 结果
     */
    @Override
    public int deleteGroupById(String groupId)
    {
        return groupMapper.deleteGroupById(groupId);
    }
}
