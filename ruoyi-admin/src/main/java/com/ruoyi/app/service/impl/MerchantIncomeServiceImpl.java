package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MerchantIncomeMapper;
import com.ruoyi.app.domain.MerchantIncome;
import com.ruoyi.app.service.IMerchantIncomeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 推荐商户收入Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MerchantIncomeServiceImpl implements IMerchantIncomeService 
{
    @Autowired
    private MerchantIncomeMapper merchantIncomeMapper;

    /**
     * 查询推荐商户收入
     * 
     * @param merIncomeId 推荐商户收入ID
     * @return 推荐商户收入
     */
    @Override
    public MerchantIncome selectMerchantIncomeById(String merIncomeId)
    {
        return merchantIncomeMapper.selectMerchantIncomeById(merIncomeId);
    }

    /**
     * 查询推荐商户收入列表
     * 
     * @param merchantIncome 推荐商户收入
     * @return 推荐商户收入
     */
    @Override
    public List<MerchantIncome> selectMerchantIncomeList(MerchantIncome merchantIncome)
    {
        return merchantIncomeMapper.selectMerchantIncomeList(merchantIncome);
    }

    /**
     * 新增推荐商户收入
     * 
     * @param merchantIncome 推荐商户收入
     * @return 结果
     */
    @Override
    public int insertMerchantIncome(MerchantIncome merchantIncome)
    {
        merchantIncome.setCreateTime(DateUtils.getNowDate());
        return merchantIncomeMapper.insertMerchantIncome(merchantIncome);
    }

    /**
     * 修改推荐商户收入
     * 
     * @param merchantIncome 推荐商户收入
     * @return 结果
     */
    @Override
    public int updateMerchantIncome(MerchantIncome merchantIncome)
    {
        return merchantIncomeMapper.updateMerchantIncome(merchantIncome);
    }

    /**
     * 删除推荐商户收入对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMerchantIncomeByIds(String ids)
    {
        return merchantIncomeMapper.deleteMerchantIncomeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除推荐商户收入信息
     * 
     * @param merIncomeId 推荐商户收入ID
     * @return 结果
     */
    @Override
    public int deleteMerchantIncomeById(String merIncomeId)
    {
        return merchantIncomeMapper.deleteMerchantIncomeById(merIncomeId);
    }
}
