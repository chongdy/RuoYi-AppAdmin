package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.PlazaContentMapper;
import com.ruoyi.app.domain.PlazaContent;
import com.ruoyi.app.service.IPlazaContentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 广场动态Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class PlazaContentServiceImpl implements IPlazaContentService 
{
    @Autowired
    private PlazaContentMapper plazaContentMapper;

    /**
     * 查询广场动态
     * 
     * @param contentId 广场动态ID
     * @return 广场动态
     */
    @Override
    public PlazaContent selectPlazaContentById(String contentId)
    {
        return plazaContentMapper.selectPlazaContentById(contentId);
    }

    /**
     * 查询广场动态列表
     * 
     * @param plazaContent 广场动态
     * @return 广场动态
     */
    @Override
    public List<PlazaContent> selectPlazaContentList(PlazaContent plazaContent)
    {
        return plazaContentMapper.selectPlazaContentList(plazaContent);
    }

    /**
     * 新增广场动态
     * 
     * @param plazaContent 广场动态
     * @return 结果
     */
    @Override
    public int insertPlazaContent(PlazaContent plazaContent)
    {
        plazaContent.setCreateTime(DateUtils.getNowDate());
        return plazaContentMapper.insertPlazaContent(plazaContent);
    }

    /**
     * 修改广场动态
     * 
     * @param plazaContent 广场动态
     * @return 结果
     */
    @Override
    public int updatePlazaContent(PlazaContent plazaContent)
    {
        return plazaContentMapper.updatePlazaContent(plazaContent);
    }

    /**
     * 删除广场动态对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePlazaContentByIds(String ids)
    {
        return plazaContentMapper.deletePlazaContentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除广场动态信息
     * 
     * @param contentId 广场动态ID
     * @return 结果
     */
    @Override
    public int deletePlazaContentById(String contentId)
    {
        return plazaContentMapper.deletePlazaContentById(contentId);
    }
}
