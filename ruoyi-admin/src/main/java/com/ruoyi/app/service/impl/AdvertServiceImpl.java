package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.AdvertMapper;
import com.ruoyi.app.domain.Advert;
import com.ruoyi.app.service.IAdvertService;
import com.ruoyi.common.core.text.Convert;

/**
 * 广告Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class AdvertServiceImpl implements IAdvertService 
{
    @Autowired
    private AdvertMapper advertMapper;

    /**
     * 查询广告
     * 
     * @param adId 广告ID
     * @return 广告
     */
    @Override
    public Advert selectAdvertById(Long adId)
    {
        return advertMapper.selectAdvertById(adId);
    }

    /**
     * 查询广告列表
     * 
     * @param advert 广告
     * @return 广告
     */
    @Override
    public List<Advert> selectAdvertList(Advert advert)
    {
        return advertMapper.selectAdvertList(advert);
    }

    /**
     * 新增广告
     * 
     * @param advert 广告
     * @return 结果
     */
    @Override
    public int insertAdvert(Advert advert)
    {
        advert.setCreateTime(DateUtils.getNowDate());
        return advertMapper.insertAdvert(advert);
    }

    /**
     * 修改广告
     * 
     * @param advert 广告
     * @return 结果
     */
    @Override
    public int updateAdvert(Advert advert)
    {
        return advertMapper.updateAdvert(advert);
    }

    /**
     * 删除广告对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteAdvertByIds(String ids)
    {
        return advertMapper.deleteAdvertByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除广告信息
     * 
     * @param adId 广告ID
     * @return 结果
     */
    @Override
    public int deleteAdvertById(Long adId)
    {
        return advertMapper.deleteAdvertById(adId);
    }
}
