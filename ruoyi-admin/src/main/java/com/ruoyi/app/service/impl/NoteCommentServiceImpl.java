package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.NoteCommentMapper;
import com.ruoyi.app.domain.NoteComment;
import com.ruoyi.app.service.INoteCommentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 游记评论Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class NoteCommentServiceImpl implements INoteCommentService 
{
    @Autowired
    private NoteCommentMapper noteCommentMapper;

    /**
     * 查询游记评论
     * 
     * @param noteId 游记评论ID
     * @return 游记评论
     */
    @Override
    public NoteComment selectNoteCommentById(String noteId)
    {
        return noteCommentMapper.selectNoteCommentById(noteId);
    }

    /**
     * 查询游记评论列表
     * 
     * @param noteComment 游记评论
     * @return 游记评论
     */
    @Override
    public List<NoteComment> selectNoteCommentList(NoteComment noteComment)
    {
        return noteCommentMapper.selectNoteCommentList(noteComment);
    }

    /**
     * 新增游记评论
     * 
     * @param noteComment 游记评论
     * @return 结果
     */
    @Override
    public int insertNoteComment(NoteComment noteComment)
    {
        noteComment.setCreateTime(DateUtils.getNowDate());
        return noteCommentMapper.insertNoteComment(noteComment);
    }

    /**
     * 修改游记评论
     * 
     * @param noteComment 游记评论
     * @return 结果
     */
    @Override
    public int updateNoteComment(NoteComment noteComment)
    {
        return noteCommentMapper.updateNoteComment(noteComment);
    }

    /**
     * 删除游记评论对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNoteCommentByIds(String ids)
    {
        return noteCommentMapper.deleteNoteCommentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除游记评论信息
     * 
     * @param noteId 游记评论ID
     * @return 结果
     */
    @Override
    public int deleteNoteCommentById(String noteId)
    {
        return noteCommentMapper.deleteNoteCommentById(noteId);
    }
}
