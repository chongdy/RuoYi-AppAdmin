package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.VersionRecMapper;
import com.ruoyi.app.domain.VersionRec;
import com.ruoyi.app.service.IVersionRecService;
import com.ruoyi.common.core.text.Convert;

/**
 * app版本Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class VersionRecServiceImpl implements IVersionRecService 
{
    @Autowired
    private VersionRecMapper versionRecMapper;

    /**
     * 查询app版本
     * 
     * @param verId app版本ID
     * @return app版本
     */
    @Override
    public VersionRec selectVersionRecById(String verId)
    {
        return versionRecMapper.selectVersionRecById(verId);
    }

    /**
     * 查询app版本列表
     * 
     * @param versionRec app版本
     * @return app版本
     */
    @Override
    public List<VersionRec> selectVersionRecList(VersionRec versionRec)
    {
        return versionRecMapper.selectVersionRecList(versionRec);
    }

    /**
     * 新增app版本
     * 
     * @param versionRec app版本
     * @return 结果
     */
    @Override
    public int insertVersionRec(VersionRec versionRec)
    {
        versionRec.setCreateTime(DateUtils.getNowDate());
        return versionRecMapper.insertVersionRec(versionRec);
    }

    /**
     * 修改app版本
     * 
     * @param versionRec app版本
     * @return 结果
     */
    @Override
    public int updateVersionRec(VersionRec versionRec)
    {
        return versionRecMapper.updateVersionRec(versionRec);
    }

    /**
     * 删除app版本对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteVersionRecByIds(String ids)
    {
        return versionRecMapper.deleteVersionRecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除app版本信息
     * 
     * @param verId app版本ID
     * @return 结果
     */
    @Override
    public int deleteVersionRecById(String verId)
    {
        return versionRecMapper.deleteVersionRecById(verId);
    }
}
