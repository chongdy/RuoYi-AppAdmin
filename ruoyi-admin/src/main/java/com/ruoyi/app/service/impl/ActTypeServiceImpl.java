package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.ActTypeMapper;
import com.ruoyi.app.domain.ActType;
import com.ruoyi.app.service.IActTypeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 活动类型Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class ActTypeServiceImpl implements IActTypeService 
{
    @Autowired
    private ActTypeMapper actTypeMapper;

    /**
     * 查询活动类型
     * 
     * @param actTypeId 活动类型ID
     * @return 活动类型
     */
    @Override
    public ActType selectActTypeById(Long actTypeId)
    {
        return actTypeMapper.selectActTypeById(actTypeId);
    }

    /**
     * 查询活动类型列表
     * 
     * @param actType 活动类型
     * @return 活动类型
     */
    @Override
    public List<ActType> selectActTypeList(ActType actType)
    {
        return actTypeMapper.selectActTypeList(actType);
    }

    /**
     * 新增活动类型
     * 
     * @param actType 活动类型
     * @return 结果
     */
    @Override
    public int insertActType(ActType actType)
    {
        actType.setCreateTime(DateUtils.getNowDate());
        return actTypeMapper.insertActType(actType);
    }

    /**
     * 修改活动类型
     * 
     * @param actType 活动类型
     * @return 结果
     */
    @Override
    public int updateActType(ActType actType)
    {
        return actTypeMapper.updateActType(actType);
    }

    /**
     * 删除活动类型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActTypeByIds(String ids)
    {
        return actTypeMapper.deleteActTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除活动类型信息
     * 
     * @param actTypeId 活动类型ID
     * @return 结果
     */
    @Override
    public int deleteActTypeById(Long actTypeId)
    {
        return actTypeMapper.deleteActTypeById(actTypeId);
    }
}
