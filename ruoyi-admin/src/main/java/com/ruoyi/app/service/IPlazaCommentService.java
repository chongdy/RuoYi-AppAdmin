package com.ruoyi.app.service;

import com.ruoyi.app.domain.PlazaComment;
import java.util.List;

/**
 * 广场评论Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IPlazaCommentService 
{
    /**
     * 查询广场评论
     * 
     * @param plazaCommId 广场评论ID
     * @return 广场评论
     */
    public PlazaComment selectPlazaCommentById(String plazaCommId);

    /**
     * 查询广场评论列表
     * 
     * @param plazaComment 广场评论
     * @return 广场评论集合
     */
    public List<PlazaComment> selectPlazaCommentList(PlazaComment plazaComment);

    /**
     * 新增广场评论
     * 
     * @param plazaComment 广场评论
     * @return 结果
     */
    public int insertPlazaComment(PlazaComment plazaComment);

    /**
     * 修改广场评论
     * 
     * @param plazaComment 广场评论
     * @return 结果
     */
    public int updatePlazaComment(PlazaComment plazaComment);

    /**
     * 批量删除广场评论
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlazaCommentByIds(String ids);

    /**
     * 删除广场评论信息
     * 
     * @param plazaCommId 广场评论ID
     * @return 结果
     */
    public int deletePlazaCommentById(String plazaCommId);
}
