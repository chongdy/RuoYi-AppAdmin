package com.ruoyi.app.service;

import com.ruoyi.app.domain.Report;
import java.util.List;

/**
 * 举报Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IReportService 
{
    /**
     * 查询举报
     * 
     * @param reportId 举报ID
     * @return 举报
     */
    public Report selectReportById(Long reportId);

    /**
     * 查询举报列表
     * 
     * @param report 举报
     * @return 举报集合
     */
    public List<Report> selectReportList(Report report);

    /**
     * 新增举报
     * 
     * @param report 举报
     * @return 结果
     */
    public int insertReport(Report report);

    /**
     * 修改举报
     * 
     * @param report 举报
     * @return 结果
     */
    public int updateReport(Report report);

    /**
     * 批量删除举报
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteReportByIds(String ids);

    /**
     * 删除举报信息
     * 
     * @param reportId 举报ID
     * @return 结果
     */
    public int deleteReportById(Long reportId);
}
