package com.ruoyi.app.service;

import com.ruoyi.app.domain.MemberOnline;
import java.util.List;

/**
 * 会员在线Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IMemberOnlineService 
{
    /**
     * 查询会员在线
     * 
     * @param onlineId 会员在线ID
     * @return 会员在线
     */
    public MemberOnline selectMemberOnlineById(Long onlineId);

    /**
     * 查询会员在线列表
     * 
     * @param memberOnline 会员在线
     * @return 会员在线集合
     */
    public List<MemberOnline> selectMemberOnlineList(MemberOnline memberOnline);

    /**
     * 新增会员在线
     * 
     * @param memberOnline 会员在线
     * @return 结果
     */
    public int insertMemberOnline(MemberOnline memberOnline);

    /**
     * 修改会员在线
     * 
     * @param memberOnline 会员在线
     * @return 结果
     */
    public int updateMemberOnline(MemberOnline memberOnline);

    /**
     * 批量删除会员在线
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberOnlineByIds(String ids);

    /**
     * 删除会员在线信息
     * 
     * @param onlineId 会员在线ID
     * @return 结果
     */
    public int deleteMemberOnlineById(Long onlineId);
}
