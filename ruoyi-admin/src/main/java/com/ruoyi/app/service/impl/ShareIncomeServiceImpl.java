package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.ShareIncomeMapper;
import com.ruoyi.app.domain.ShareIncome;
import com.ruoyi.app.service.IShareIncomeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 分享新用户收入Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class ShareIncomeServiceImpl implements IShareIncomeService 
{
    @Autowired
    private ShareIncomeMapper shareIncomeMapper;

    /**
     * 查询分享新用户收入
     * 
     * @param shareIncomeId 分享新用户收入ID
     * @return 分享新用户收入
     */
    @Override
    public ShareIncome selectShareIncomeById(String shareIncomeId)
    {
        return shareIncomeMapper.selectShareIncomeById(shareIncomeId);
    }

    /**
     * 查询分享新用户收入列表
     * 
     * @param shareIncome 分享新用户收入
     * @return 分享新用户收入
     */
    @Override
    public List<ShareIncome> selectShareIncomeList(ShareIncome shareIncome)
    {
        return shareIncomeMapper.selectShareIncomeList(shareIncome);
    }

    /**
     * 新增分享新用户收入
     * 
     * @param shareIncome 分享新用户收入
     * @return 结果
     */
    @Override
    public int insertShareIncome(ShareIncome shareIncome)
    {
        shareIncome.setCreateTime(DateUtils.getNowDate());
        return shareIncomeMapper.insertShareIncome(shareIncome);
    }

    /**
     * 修改分享新用户收入
     * 
     * @param shareIncome 分享新用户收入
     * @return 结果
     */
    @Override
    public int updateShareIncome(ShareIncome shareIncome)
    {
        return shareIncomeMapper.updateShareIncome(shareIncome);
    }

    /**
     * 删除分享新用户收入对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShareIncomeByIds(String ids)
    {
        return shareIncomeMapper.deleteShareIncomeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除分享新用户收入信息
     * 
     * @param shareIncomeId 分享新用户收入ID
     * @return 结果
     */
    @Override
    public int deleteShareIncomeById(String shareIncomeId)
    {
        return shareIncomeMapper.deleteShareIncomeById(shareIncomeId);
    }
}
