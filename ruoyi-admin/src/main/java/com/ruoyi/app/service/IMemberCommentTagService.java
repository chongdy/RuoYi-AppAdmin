package com.ruoyi.app.service;

import com.ruoyi.app.domain.MemberCommentTag;
import java.util.List;

/**
 * 会员评价标签Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IMemberCommentTagService 
{
    /**
     * 查询会员评价标签
     * 
     * @param tagId 会员评价标签ID
     * @return 会员评价标签
     */
    public MemberCommentTag selectMemberCommentTagById(Long tagId);

    /**
     * 查询会员评价标签列表
     * 
     * @param memberCommentTag 会员评价标签
     * @return 会员评价标签集合
     */
    public List<MemberCommentTag> selectMemberCommentTagList(MemberCommentTag memberCommentTag);

    /**
     * 新增会员评价标签
     * 
     * @param memberCommentTag 会员评价标签
     * @return 结果
     */
    public int insertMemberCommentTag(MemberCommentTag memberCommentTag);

    /**
     * 修改会员评价标签
     * 
     * @param memberCommentTag 会员评价标签
     * @return 结果
     */
    public int updateMemberCommentTag(MemberCommentTag memberCommentTag);

    /**
     * 批量删除会员评价标签
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberCommentTagByIds(String ids);

    /**
     * 删除会员评价标签信息
     * 
     * @param tagId 会员评价标签ID
     * @return 结果
     */
    public int deleteMemberCommentTagById(Long tagId);
}
