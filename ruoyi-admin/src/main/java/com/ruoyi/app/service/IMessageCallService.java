package com.ruoyi.app.service;

import com.ruoyi.app.domain.MessageCall;
import java.util.List;

/**
 * 招呼Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IMessageCallService 
{
    /**
     * 查询招呼
     * 
     * @param callId 招呼ID
     * @return 招呼
     */
    public MessageCall selectMessageCallById(String callId);

    /**
     * 查询招呼列表
     * 
     * @param messageCall 招呼
     * @return 招呼集合
     */
    public List<MessageCall> selectMessageCallList(MessageCall messageCall);

    /**
     * 新增招呼
     * 
     * @param messageCall 招呼
     * @return 结果
     */
    public int insertMessageCall(MessageCall messageCall);

    /**
     * 修改招呼
     * 
     * @param messageCall 招呼
     * @return 结果
     */
    public int updateMessageCall(MessageCall messageCall);

    /**
     * 批量删除招呼
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMessageCallByIds(String ids);

    /**
     * 删除招呼信息
     * 
     * @param callId 招呼ID
     * @return 结果
     */
    public int deleteMessageCallById(String callId);
}
