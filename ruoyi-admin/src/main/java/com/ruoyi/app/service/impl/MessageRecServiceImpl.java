package com.ruoyi.app.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MessageRecMapper;
import com.ruoyi.app.domain.MessageRec;
import com.ruoyi.app.service.IMessageRecService;
import com.ruoyi.common.core.text.Convert;

/**
 * 消息记录Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MessageRecServiceImpl implements IMessageRecService 
{
    @Autowired
    private MessageRecMapper messageRecMapper;

    /**
     * 查询消息记录
     * 
     * @param msgId 消息记录ID
     * @return 消息记录
     */
    @Override
    public MessageRec selectMessageRecById(String msgId)
    {
        return messageRecMapper.selectMessageRecById(msgId);
    }

    /**
     * 查询消息记录列表
     * 
     * @param messageRec 消息记录
     * @return 消息记录
     */
    @Override
    public List<MessageRec> selectMessageRecList(MessageRec messageRec)
    {
        return messageRecMapper.selectMessageRecList(messageRec);
    }

    /**
     * 新增消息记录
     * 
     * @param messageRec 消息记录
     * @return 结果
     */
    @Override
    public int insertMessageRec(MessageRec messageRec)
    {
        return messageRecMapper.insertMessageRec(messageRec);
    }

    /**
     * 修改消息记录
     * 
     * @param messageRec 消息记录
     * @return 结果
     */
    @Override
    public int updateMessageRec(MessageRec messageRec)
    {
        return messageRecMapper.updateMessageRec(messageRec);
    }

    /**
     * 删除消息记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMessageRecByIds(String ids)
    {
        return messageRecMapper.deleteMessageRecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除消息记录信息
     * 
     * @param msgId 消息记录ID
     * @return 结果
     */
    @Override
    public int deleteMessageRecById(String msgId)
    {
        return messageRecMapper.deleteMessageRecById(msgId);
    }
}
