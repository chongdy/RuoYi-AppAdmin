package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.ActCommentMapper;
import com.ruoyi.app.domain.ActComment;
import com.ruoyi.app.service.IActCommentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 活动评价Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class ActCommentServiceImpl implements IActCommentService 
{
    @Autowired
    private ActCommentMapper actCommentMapper;

    /**
     * 查询活动评价
     * 
     * @param actCommId 活动评价ID
     * @return 活动评价
     */
    @Override
    public ActComment selectActCommentById(Long actCommId)
    {
        return actCommentMapper.selectActCommentById(actCommId);
    }

    /**
     * 查询活动评价列表
     * 
     * @param actComment 活动评价
     * @return 活动评价
     */
    @Override
    public List<ActComment> selectActCommentList(ActComment actComment)
    {
        return actCommentMapper.selectActCommentList(actComment);
    }

    /**
     * 新增活动评价
     * 
     * @param actComment 活动评价
     * @return 结果
     */
    @Override
    public int insertActComment(ActComment actComment)
    {
        actComment.setCreateTime(DateUtils.getNowDate());
        return actCommentMapper.insertActComment(actComment);
    }

    /**
     * 修改活动评价
     * 
     * @param actComment 活动评价
     * @return 结果
     */
    @Override
    public int updateActComment(ActComment actComment)
    {
        return actCommentMapper.updateActComment(actComment);
    }

    /**
     * 删除活动评价对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActCommentByIds(String ids)
    {
        return actCommentMapper.deleteActCommentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除活动评价信息
     * 
     * @param actCommId 活动评价ID
     * @return 结果
     */
    @Override
    public int deleteActCommentById(Long actCommId)
    {
        return actCommentMapper.deleteActCommentById(actCommId);
    }
}
