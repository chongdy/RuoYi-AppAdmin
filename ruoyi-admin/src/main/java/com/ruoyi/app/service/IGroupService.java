package com.ruoyi.app.service;

import com.ruoyi.app.domain.Group;
import java.util.List;

/**
 * 微信群Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IGroupService 
{
    /**
     * 查询微信群
     * 
     * @param groupId 微信群ID
     * @return 微信群
     */
    public Group selectGroupById(String groupId);

    /**
     * 查询微信群列表
     * 
     * @param group 微信群
     * @return 微信群集合
     */
    public List<Group> selectGroupList(Group group);

    /**
     * 新增微信群
     * 
     * @param group 微信群
     * @return 结果
     */
    public int insertGroup(Group group);

    /**
     * 修改微信群
     * 
     * @param group 微信群
     * @return 结果
     */
    public int updateGroup(Group group);

    /**
     * 批量删除微信群
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupByIds(String ids);

    /**
     * 删除微信群信息
     * 
     * @param groupId 微信群ID
     * @return 结果
     */
    public int deleteGroupById(String groupId);
}
