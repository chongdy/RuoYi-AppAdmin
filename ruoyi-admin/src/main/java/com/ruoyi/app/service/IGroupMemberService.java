package com.ruoyi.app.service;

import com.ruoyi.app.domain.GroupMember;
import java.util.List;

/**
 * 加群记录Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IGroupMemberService 
{
    /**
     * 查询加群记录
     * 
     * @param agmId 加群记录ID
     * @return 加群记录
     */
    public GroupMember selectGroupMemberById(String agmId);

    /**
     * 查询加群记录列表
     * 
     * @param groupMember 加群记录
     * @return 加群记录集合
     */
    public List<GroupMember> selectGroupMemberList(GroupMember groupMember);

    /**
     * 新增加群记录
     * 
     * @param groupMember 加群记录
     * @return 结果
     */
    public int insertGroupMember(GroupMember groupMember);

    /**
     * 修改加群记录
     * 
     * @param groupMember 加群记录
     * @return 结果
     */
    public int updateGroupMember(GroupMember groupMember);

    /**
     * 批量删除加群记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupMemberByIds(String ids);

    /**
     * 删除加群记录信息
     * 
     * @param agmId 加群记录ID
     * @return 结果
     */
    public int deleteGroupMemberById(String agmId);
}
