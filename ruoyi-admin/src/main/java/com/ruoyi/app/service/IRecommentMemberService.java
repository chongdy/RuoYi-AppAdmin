package com.ruoyi.app.service;

import com.ruoyi.app.domain.RecommentMember;
import java.util.List;

/**
 * 推荐会员Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IRecommentMemberService 
{
    /**
     * 查询推荐会员
     * 
     * @param recId 推荐会员ID
     * @return 推荐会员
     */
    public RecommentMember selectRecommentMemberById(String recId);

    /**
     * 查询推荐会员列表
     * 
     * @param recommentMember 推荐会员
     * @return 推荐会员集合
     */
    public List<RecommentMember> selectRecommentMemberList(RecommentMember recommentMember);

    /**
     * 新增推荐会员
     * 
     * @param recommentMember 推荐会员
     * @return 结果
     */
    public int insertRecommentMember(RecommentMember recommentMember);

    /**
     * 修改推荐会员
     * 
     * @param recommentMember 推荐会员
     * @return 结果
     */
    public int updateRecommentMember(RecommentMember recommentMember);

    /**
     * 批量删除推荐会员
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecommentMemberByIds(String ids);

    /**
     * 删除推荐会员信息
     * 
     * @param recId 推荐会员ID
     * @return 结果
     */
    public int deleteRecommentMemberById(String recId);
}
