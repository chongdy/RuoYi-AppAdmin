package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.CollectActMapper;
import com.ruoyi.app.domain.CollectAct;
import com.ruoyi.app.service.ICollectActService;
import com.ruoyi.common.core.text.Convert;

/**
 * 收藏活动Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class CollectActServiceImpl implements ICollectActService 
{
    @Autowired
    private CollectActMapper collectActMapper;

    /**
     * 查询收藏活动
     * 
     * @param collectId 收藏活动ID
     * @return 收藏活动
     */
    @Override
    public CollectAct selectCollectActById(Long collectId)
    {
        return collectActMapper.selectCollectActById(collectId);
    }

    /**
     * 查询收藏活动列表
     * 
     * @param collectAct 收藏活动
     * @return 收藏活动
     */
    @Override
    public List<CollectAct> selectCollectActList(CollectAct collectAct)
    {
        return collectActMapper.selectCollectActList(collectAct);
    }

    /**
     * 新增收藏活动
     * 
     * @param collectAct 收藏活动
     * @return 结果
     */
    @Override
    public int insertCollectAct(CollectAct collectAct)
    {
        collectAct.setCreateTime(DateUtils.getNowDate());
        return collectActMapper.insertCollectAct(collectAct);
    }

    /**
     * 修改收藏活动
     * 
     * @param collectAct 收藏活动
     * @return 结果
     */
    @Override
    public int updateCollectAct(CollectAct collectAct)
    {
        return collectActMapper.updateCollectAct(collectAct);
    }

    /**
     * 删除收藏活动对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCollectActByIds(String ids)
    {
        return collectActMapper.deleteCollectActByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除收藏活动信息
     * 
     * @param collectId 收藏活动ID
     * @return 结果
     */
    @Override
    public int deleteCollectActById(Long collectId)
    {
        return collectActMapper.deleteCollectActById(collectId);
    }
}
